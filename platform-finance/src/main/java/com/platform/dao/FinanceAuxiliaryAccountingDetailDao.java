package com.platform.dao;

import com.platform.entity.FinanceAuxiliaryAccountingDetailEntity;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2018-07-02 10:21:17
 */
public interface FinanceAuxiliaryAccountingDetailDao extends BaseDao<FinanceAuxiliaryAccountingDetailEntity> {

}
