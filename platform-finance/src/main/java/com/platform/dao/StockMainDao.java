package com.platform.dao;

import com.platform.entity.StockMainEntity;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-02-12 11:58:53
 */
public interface StockMainDao extends BaseDao<StockMainEntity> {

}
