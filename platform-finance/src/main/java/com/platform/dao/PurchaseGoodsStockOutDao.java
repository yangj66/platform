package com.platform.dao;

import com.platform.entity.PurchaseGoodsStockOutEntity;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-02-18 21:26:57
 */
public interface PurchaseGoodsStockOutDao extends BaseDao<PurchaseGoodsStockOutEntity> {
    void saveMap(Map<String, Object> map);
    int undoBatch(Object[] id);
    void count(@Param("keyIds")Object[] keyIds, @Param("unitPrice")Double unitPrice);
    String getMaxId(String prefix);
}
