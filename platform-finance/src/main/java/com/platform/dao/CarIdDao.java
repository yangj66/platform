package com.platform.dao;

import com.platform.entity.CarIdEntity;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-04-25 10:48:02
 */
public interface CarIdDao extends BaseDao<CarIdEntity> {

}
