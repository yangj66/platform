package com.platform.dao;

import com.platform.entity.PurchaseGoodsMainEntity;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-02-19 09:02:41
 */
public interface PurchaseGoodsMainDao extends BaseDao<PurchaseGoodsMainEntity> {

}
