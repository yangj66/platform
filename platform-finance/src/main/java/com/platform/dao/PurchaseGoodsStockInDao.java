package com.platform.dao;

import com.platform.entity.PurchaseGoodsStockInEntity;

import java.util.Map;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-02-12 13:25:53
 */
public interface PurchaseGoodsStockInDao extends BaseDao<PurchaseGoodsStockInEntity> {
    void saveMap(Map<String, Object> map);
    void deleteByStockInId(String stockInId);
    String getMaxId(String prefix);
}
