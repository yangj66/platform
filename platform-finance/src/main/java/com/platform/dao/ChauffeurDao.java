package com.platform.dao;

import com.platform.entity.ChauffeurEntity;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-04-25 10:48:03
 */
public interface ChauffeurDao extends BaseDao<ChauffeurEntity> {

}
