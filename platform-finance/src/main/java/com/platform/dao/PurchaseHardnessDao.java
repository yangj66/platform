package com.platform.dao;

import com.platform.entity.PurchaseHardnessEntity;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-02-18 21:26:57
 */
public interface PurchaseHardnessDao extends BaseDao<PurchaseHardnessEntity> {

}
