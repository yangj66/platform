package com.platform.dao;

import com.platform.entity.FinanceAuxiliaryAccountingEntity;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2018-07-02 16:23:52
 */
public interface FinanceAuxiliaryAccountingDao extends BaseDao<FinanceAuxiliaryAccountingEntity> {

}
