package com.platform.dao;

import com.platform.entity.ProjectMainEntity;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-03-18 10:21:37
 */
public interface ProjectMainDao extends BaseDao<ProjectMainEntity> {

}
