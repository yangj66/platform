package com.platform.dao;

import com.platform.entity.SupplierMainEntity;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-02-15 15:15:32
 */
public interface SupplierMainDao extends BaseDao<SupplierMainEntity> {

}
