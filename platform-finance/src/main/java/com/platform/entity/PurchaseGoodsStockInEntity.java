package com.platform.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 实体
 * 表名 purchase_goods_stock_in
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-02-12 13:25:53
 */
public class PurchaseGoodsStockInEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    //
    private Long keyId;
    //入库编号
    private String stockInId;
    //操作时间
    private Date inputDate;
    //入库时间
    private Date stockInDate;
    //入库人
    private String stockInResponsier;
    //仓库ID
    private String stockId;
    //供应商
    private String supplierId;
    //备注
    private String stockInMemo;
    //条目号
    private Integer itemNumber;
    //代码
    private String productId;
    //库存数量
    private Double stockQuantity;
    //条目备注
    private String itemMemo;
    //入库数量
    private Double inQuantity;
    //出库数量
    private Double outQuantity;
    //状态
    private String status;

    //条目号，代码，入库数量，条目备注的list
    private List list;

    //翻译字段
    private String productName;
    private String stockDescription;
    private String username;
    private String batchNumber;
    private String pProductMeasurement;
    private String pProductName;
    private String pProductDescription;
    private String pProductMemo;
    private String pProductClass;
    private String pGuige;
    private String pDaima;

    public String getpProductMeasurement() {
        return pProductMeasurement;
    }

    public void setpProductMeasurement(String pProductMeasurement) {
        this.pProductMeasurement = pProductMeasurement;
    }

    public String getpProductMemo() {
        return pProductMemo;
    }

    public void setpProductMemo(String pProductMemo) {
        this.pProductMemo = pProductMemo;
    }

    public String getpGuige() {
        return pGuige;
    }

    public void setpGuige(String pGuige) {
        this.pGuige = pGuige;
    }

    public String getpDaima() {
        return pDaima;
    }

    public void setpDaima(String pDaima) {
        this.pDaima = pDaima;
    }

    public String getpProductName() {
        return pProductName;
    }

    public void setpProductName(String pProductName) {
        this.pProductName = pProductName;
    }

    public String getpProductClass() {
        return pProductClass;
    }

    public void setpProductClass(String pProductClass) {
        this.pProductClass = pProductClass;
    }

    public String getpProductDescription() {
        return pProductDescription;
    }

    public void setpProductDescription(String pProductDescription) {
        this.pProductDescription = pProductDescription;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public Double getInQuantity() {
        return inQuantity;
    }

    public void setInQuantity(Double inQuantity) {
        this.inQuantity = inQuantity;
    }

    public Double getOutQuantity() {
        return outQuantity;
    }

    public void setOutQuantity(Double outQuantity) {
        this.outQuantity = outQuantity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getStockDescription() {
        return stockDescription;
    }

    public void setStockDescription(String stockDescription) {
        this.stockDescription = stockDescription;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    /**
     * 设置：
     */
    public void setKeyId(Long keyId) {
        this.keyId = keyId;
    }

    /**
     * 获取：
     */
    public Long getKeyId() {
        return keyId;
    }
    /**
     * 设置：入库编号
     */
    public void setStockInId(String stockInId) {
        this.stockInId = stockInId;
    }

    /**
     * 获取：入库编号
     */
    public String getStockInId() {
        return stockInId;
    }

    public Date getInputDate() {
        return inputDate;
    }

    public void setInputDate(Date inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * 设置：入库时间
     */
    public void setStockInDate(Date stockInDate) {
        this.stockInDate = stockInDate;
    }

    /**
     * 获取：入库时间
     */
    public Date getStockInDate() {
        return stockInDate;
    }
    /**
     * 设置：入库人
     */
    public void setStockInResponsier(String stockInResponsier) {
        this.stockInResponsier = stockInResponsier;
    }

    /**
     * 获取：入库人
     */
    public String getStockInResponsier() {
        return stockInResponsier;
    }
    /**
     * 设置：仓库ID
     */
    public void setStockId(String stockId) {
        this.stockId = stockId;
    }

    /**
     * 获取：仓库ID
     */
    public String getStockId() {
        return stockId;
    }
    /**
     * 设置：供应商
     */
    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    /**
     * 获取：供应商
     */
    public String getSupplierId() {
        return supplierId;
    }
    /**
     * 设置：备注
     */
    public void setStockInMemo(String stockInMemo) {
        this.stockInMemo = stockInMemo;
    }

    /**
     * 获取：备注
     */
    public String getStockInMemo() {
        return stockInMemo;
    }
    /**
     * 设置：条目号
     */
    public void setItemNumber(Integer itemNumber) {
        this.itemNumber = itemNumber;
    }

    /**
     * 获取：条目号
     */
    public Integer getItemNumber() {
        return itemNumber;
    }
    /**
     * 设置：代码
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     * 获取：代码
     */
    public String getProductId() {
        return productId;
    }
    /**
     * 设置：入库数量
     */
    public void setStockQuantity(Double stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    /**
     * 获取：入库数量
     */
    public Double getStockQuantity() {
        return stockQuantity;
    }
    /**
     * 设置：条目备注
     */
    public void setItemMemo(String itemMemo) {
        this.itemMemo = itemMemo;
    }

    /**
     * 获取：条目备注
     */
    public String getItemMemo() {
        return itemMemo;
    }
}
