package com.platform.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 实体
 * 表名 purchase_goods_stock_out
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-02-18 21:26:57
 */
public class PurchaseGoodsStockOutEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    //
    private Long keyId;
    //出库编号
    private String stockOutId;
    //入库keyId
    private Long stockInKeyId;
    //入库编号
    private String stockInId;
    //入库条目号
    private Integer stockInItemNumber;
    //出库日期
    private Date stockOutDate;
    //录入时间
    private Date inputDate;
    //出库责任人
    private String stockOutResponsier;
    //仓库ID
    private String stockId;
    //净重
    private Double stockOutQuantity;
    //物品ID
    private String productId;
    //供应商ID
    private String supplierId;
    //出库备注
    private String stockOutMemo;
    //冲单标志
    private Integer withdrawFlag;
    //出库操作人
    private String stockOutOperatorId;
    //毛重
    private Double grossWeight;
    //皮重
    private Double tareWeight;
    //方量
    private Double netWeight;
    //强度
    private String hardness;
    //联系电话
    private String telephone;
    //联系人
    private String contacter;
    //承运
    private String carId;
    //司机
    private String chauffeur;
    //送货地址
    private String address;
    //项目名称
    private String projectName;
    //单价
    private Double unitPrice;
    //金额
    private Double price;
    //条目备注
    private String itemMemo;

    //发货人
    private String outPerson;

    //入库list
    private List list;

    //翻译字段
    private String productName;
    private String stockDescription;
    private String responsier;
    private String operator;
    private String supplierDescription;
    private String batchNumber;

    private String pProductMeasurement;
    private String pProductName;
    private String pProductDescription;
    private String pProductMemo;
    private String pProductClass;
    private String pGuige;
    private String pDaima;

    public String getOutPerson() {
        return outPerson;
    }

    public void setOutPerson(String outPerson) {
        this.outPerson = outPerson;
    }

    public String getpProductMeasurement() {
        return pProductMeasurement;
    }

    public void setpProductMeasurement(String pProductMeasurement) {
        this.pProductMeasurement = pProductMeasurement;
    }

    public String getpProductMemo() {
        return pProductMemo;
    }

    public void setpProductMemo(String pProductMemo) {
        this.pProductMemo = pProductMemo;
    }

    public String getpGuige() {
        return pGuige;
    }

    public void setpGuige(String pGuige) {
        this.pGuige = pGuige;
    }

    public String getpDaima() {
        return pDaima;
    }

    public void setpDaima(String pDaima) {
        this.pDaima = pDaima;
    }

    public String getItemMemo() {
        return itemMemo;
    }

    public void setItemMemo(String itemMemo) {
        this.itemMemo = itemMemo;
    }

    public String getProjectName() {
        return projectName;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getpProductName() {
        return pProductName;
    }

    public void setpProductName(String pProductName) {
        this.pProductName = pProductName;
    }

    public String getpProductClass() {
        return pProductClass;
    }

    public void setpProductClass(String pProductClass) {
        this.pProductClass = pProductClass;
    }

    public String getpProductDescription() {
        return pProductDescription;
    }

    public void setpProductDescription(String pProductDescription) {
        this.pProductDescription = pProductDescription;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getResponsier() {
        return responsier;
    }

    public void setResponsier(String responsier) {
        this.responsier = responsier;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getStockDescription() {
        return stockDescription;
    }

    public void setStockDescription(String stockDescription) {
        this.stockDescription = stockDescription;
    }

    public String getSupplierDescription() {
        return supplierDescription;
    }

    public void setSupplierDescription(String supplierDescription) {
        this.supplierDescription = supplierDescription;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    /**
     * 设置：
     */
    public void setKeyId(Long keyId) {
        this.keyId = keyId;
    }

    /**
     * 获取：
     */
    public Long getKeyId() {
        return keyId;
    }
    /**
     * 设置：出库编号
     */
    public void setStockOutId(String stockOutId) {
        this.stockOutId = stockOutId;
    }

    /**
     * 获取：出库编号
     */
    public String getStockOutId() {
        return stockOutId;
    }
    /**
     * 设置：入库keyId
     */
    public void setStockInKeyId(Long stockInKeyId) {
        this.stockInKeyId = stockInKeyId;
    }

    /**
     * 获取：入库keyId
     */
    public Long getStockInKeyId() {
        return stockInKeyId;
    }
    /**
     * 设置：入库编号
     */
    public void setStockInId(String stockInId) {
        this.stockInId = stockInId;
    }

    /**
     * 获取：入库编号
     */
    public String getStockInId() {
        return stockInId;
    }
    /**
     * 设置：入库条目号
     */
    public void setStockInItemNumber(Integer stockInItemNumber) {
        this.stockInItemNumber = stockInItemNumber;
    }

    /**
     * 获取：入库条目号
     */
    public Integer getStockInItemNumber() {
        return stockInItemNumber;
    }
    /**
     * 设置：出库日期
     */
    public void setStockOutDate(Date stockOutDate) {
        this.stockOutDate = stockOutDate;
    }

    /**
     * 获取：出库日期
     */
    public Date getStockOutDate() {
        return stockOutDate;
    }
    /**
     * 设置：录入时间
     */
    public void setInputDate(Date inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * 获取：录入时间
     */
    public Date getInputDate() {
        return inputDate;
    }
    /**
     * 设置：出库责任人
     */
    public void setStockOutResponsier(String stockOutResponsier) {
        this.stockOutResponsier = stockOutResponsier;
    }

    /**
     * 获取：出库责任人
     */
    public String getStockOutResponsier() {
        return stockOutResponsier;
    }
    /**
     * 设置：仓库ID
     */
    public void setStockId(String stockId) {
        this.stockId = stockId;
    }

    /**
     * 获取：仓库ID
     */
    public String getStockId() {
        return stockId;
    }
    /**
     * 设置：净重
     */
    public void setStockOutQuantity(Double stockOutQuantity) {
        this.stockOutQuantity = stockOutQuantity;
    }

    /**
     * 获取：净重
     */
    public Double getStockOutQuantity() {
        return stockOutQuantity;
    }
    /**
     * 设置：物品ID
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     * 获取：物品ID
     */
    public String getProductId() {
        return productId;
    }
    /**
     * 设置：供应商ID
     */
    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    /**
     * 获取：供应商ID
     */
    public String getSupplierId() {
        return supplierId;
    }
    /**
     * 设置：出库备注
     */
    public void setStockOutMemo(String stockOutMemo) {
        this.stockOutMemo = stockOutMemo;
    }

    /**
     * 获取：出库备注
     */
    public String getStockOutMemo() {
        return stockOutMemo;
    }
    /**
     * 设置：冲单标志
     */
    public void setWithdrawFlag(Integer withdrawFlag) {
        this.withdrawFlag = withdrawFlag;
    }

    /**
     * 获取：冲单标志
     */
    public Integer getWithdrawFlag() {
        return withdrawFlag;
    }
    /**
     * 设置：出库操作人
     */
    public void setStockOutOperatorId(String stockOutOperatorId) {
        this.stockOutOperatorId = stockOutOperatorId;
    }

    /**
     * 获取：出库操作人
     */
    public String getStockOutOperatorId() {
        return stockOutOperatorId;
    }
    /**
     * 设置：毛重
     */
    public void setGrossWeight(Double grossWeight) {
        this.grossWeight = grossWeight;
    }

    /**
     * 获取：毛重
     */
    public Double getGrossWeight() {
        return grossWeight;
    }
    /**
     * 设置：皮重
     */
    public void setTareWeight(Double tareWeight) {
        this.tareWeight = tareWeight;
    }

    /**
     * 获取：皮重
     */
    public Double getTareWeight() {
        return tareWeight;
    }
    /**
     * 设置：方量
     */
    public void setNetWeight(Double netWeight) {
        this.netWeight = netWeight;
    }

    /**
     * 获取：方量
     */
    public Double getNetWeight() {
        return netWeight;
    }
    /**
     * 设置：强度
     */
    public void setHardness(String hardness) {
        this.hardness = hardness;
    }

    /**
     * 获取：强度
     */
    public String getHardness() {
        return hardness;
    }
    /**
     * 设置：联系电话
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * 获取：联系电话
     */
    public String getTelephone() {
        return telephone;
    }
    /**
     * 设置：联系人
     */
    public void setContacter(String contacter) {
        this.contacter = contacter;
    }

    /**
     * 获取：联系人
     */
    public String getContacter() {
        return contacter;
    }

    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }

    /**
     * 设置：司机
     */
    public void setChauffeur(String chauffeur) {
        this.chauffeur = chauffeur;
    }

    /**
     * 获取：司机
     */
    public String getChauffeur() {
        return chauffeur;
    }
    /**
     * 设置：送货地址
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * 获取：送货地址
     */
    public String getAddress() {
        return address;
    }
}
