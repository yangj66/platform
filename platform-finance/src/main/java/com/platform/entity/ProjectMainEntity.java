package com.platform.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 实体
 * 表名 project_main
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-03-18 10:21:37
 */
public class ProjectMainEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    //
    private Long keyId;
    //项目名称
    private String projectName;
    //地址
    private String address;
    //电话
    private String tel;
    //联系人
    private String contact;
    //状态
    private String status;

    /**
     * 设置：
     */
    public void setKeyId(Long keyId) {
        this.keyId = keyId;
    }

    /**
     * 获取：
     */
    public Long getKeyId() {
        return keyId;
    }
    /**
     * 设置：项目名称
     */
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    /**
     * 获取：项目名称
     */
    public String getProjectName() {
        return projectName;
    }
    /**
     * 设置：地址
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * 获取：地址
     */
    public String getAddress() {
        return address;
    }
    /**
     * 设置：电话
     */
    public void setTel(String tel) {
        this.tel = tel;
    }

    /**
     * 获取：电话
     */
    public String getTel() {
        return tel;
    }
    /**
     * 设置：联系人
     */
    public void setContact(String contact) {
        this.contact = contact;
    }

    /**
     * 获取：联系人
     */
    public String getContact() {
        return contact;
    }
    /**
     * 设置：状态
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 获取：状态
     */
    public String getStatus() {
        return status;
    }
}
