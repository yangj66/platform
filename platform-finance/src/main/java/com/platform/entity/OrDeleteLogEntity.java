/*
 * 类名称:OrDeleteLogEntity.java
 * 包名称:com.platform.entity
 *
 * 修改履历:
 *     日期                       修正者        主要内容
 *     2019-10-09 09:10:55        lipengjun     初版做成
 *
 * Copyright (c) 2019-2019 微同软件
 */
package com.platform.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 实体
 *
 * @author lipengjun
 * @date 2019-10-09 09:10:55
 */
@Data
@TableName("update_or_delete_log")
public class OrDeleteLogEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    @TableId
    private Long id;
    /**
     * 操作类型
     */
    private String type;
    /**
     * 删除单号
     */
    private String orderId;
    /**
     * 操作人员
     */
    private Long userId;
    /**
     * 操作时间
     */
    private Date operateTime;
    /**
     * 删除的信息
     */
    private String memo;
}
