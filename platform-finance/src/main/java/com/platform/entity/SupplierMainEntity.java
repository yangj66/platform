package com.platform.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 实体
 * 表名 supplier_main
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-02-15 15:15:32
 */
public class SupplierMainEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    //
    private Long keyId;
    //代码
    private String supplierId;
    //供应商名称
    private String supplierDescription;
    //地址
    private String address;
    //电话
    private String tel;
    //状态
    private String status;

    /**
     * 设置：
     */
    public void setKeyId(Long keyId) {
        this.keyId = keyId;
    }

    /**
     * 获取：
     */
    public Long getKeyId() {
        return keyId;
    }
    /**
     * 设置：代码
     */
    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    /**
     * 获取：代码
     */
    public String getSupplierId() {
        return supplierId;
    }
    /**
     * 设置：供应商名称
     */
    public void setSupplierDescription(String supplierDescription) {
        this.supplierDescription = supplierDescription;
    }

    /**
     * 获取：供应商名称
     */
    public String getSupplierDescription() {
        return supplierDescription;
    }
    /**
     * 设置：地址
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * 获取：地址
     */
    public String getAddress() {
        return address;
    }
    /**
     * 设置：电话
     */
    public void setTel(String tel) {
        this.tel = tel;
    }

    /**
     * 获取：电话
     */
    public String getTel() {
        return tel;
    }
    /**
     * 设置：状态
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 获取：状态
     */
    public String getStatus() {
        return status;
    }
}
