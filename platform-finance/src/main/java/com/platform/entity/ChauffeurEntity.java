package com.platform.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 实体
 * 表名 chauffeur
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-04-25 10:48:03
 */
public class ChauffeurEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    //主键
    private Integer keyId;
    //司机
    private String chauffeur;

    /**
     * 设置：主键
     */
    public void setKeyId(Integer keyId) {
        this.keyId = keyId;
    }

    /**
     * 获取：主键
     */
    public Integer getKeyId() {
        return keyId;
    }
    /**
     * 设置：司机
     */
    public void setChauffeur(String chauffeur) {
        this.chauffeur = chauffeur;
    }

    /**
     * 获取：司机
     */
    public String getChauffeur() {
        return chauffeur;
    }
}
