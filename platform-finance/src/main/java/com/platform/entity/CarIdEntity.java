package com.platform.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 实体
 * 表名 car_id
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-04-25 10:48:02
 */
public class CarIdEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    //主键
    private Integer keyId;
    //承运
    private String carId;

    /**
     * 设置：主键
     */
    public void setKeyId(Integer keyId) {
        this.keyId = keyId;
    }

    /**
     * 获取：主键
     */
    public Integer getKeyId() {
        return keyId;
    }
    /**
     * 设置：承运
     */
    public void setCarId(String carId) {
        this.carId = carId;
    }

    /**
     * 获取：承运
     */
    public String getCarId() {
        return carId;
    }
}
