package com.platform.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 实体
 * 表名 out_person
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-04-25 10:48:03
 */
public class OutPersonEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    //主键
    private Integer keyId;
    //发货人
    private String outPerson;

    /**
     * 设置：主键
     */
    public void setKeyId(Integer keyId) {
        this.keyId = keyId;
    }

    /**
     * 获取：主键
     */
    public Integer getKeyId() {
        return keyId;
    }
    /**
     * 设置：发货人
     */
    public void setOutPerson(String outPerson) {
        this.outPerson = outPerson;
    }

    /**
     * 获取：发货人
     */
    public String getOutPerson() {
        return outPerson;
    }
}
