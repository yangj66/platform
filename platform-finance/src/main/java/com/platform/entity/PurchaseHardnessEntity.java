package com.platform.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 实体
 * 表名 purchase_hardness
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-02-18 21:26:57
 */
public class PurchaseHardnessEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    //
    private Integer id;
    //
    private String hardness;

    /**
     * 设置：
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取：
     */
    public Integer getId() {
        return id;
    }
    /**
     * 设置：
     */
    public void setHardness(String hardness) {
        this.hardness = hardness;
    }

    /**
     * 获取：
     */
    public String getHardness() {
        return hardness;
    }
}
