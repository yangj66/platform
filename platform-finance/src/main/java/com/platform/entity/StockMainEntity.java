package com.platform.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 实体
 * 表名 stock_main
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-02-12 11:58:53
 */
public class StockMainEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    //
    private Long keyId;
    //仓库ID
    private String stockId;
    //仓库名称
    private String stockDescription;
    //状态
    private String status;

    /**
     * 设置：
     */
    public void setKeyId(Long keyId) {
        this.keyId = keyId;
    }

    /**
     * 获取：
     */
    public Long getKeyId() {
        return keyId;
    }
    /**
     * 设置：仓库ID
     */
    public void setStockId(String stockId) {
        this.stockId = stockId;
    }

    /**
     * 获取：仓库ID
     */
    public String getStockId() {
        return stockId;
    }
    /**
     * 设置：仓库名称
     */
    public void setStockDescription(String stockDescription) {
        this.stockDescription = stockDescription;
    }

    /**
     * 获取：仓库名称
     */
    public String getStockDescription() {
        return stockDescription;
    }
    /**
     * 设置：状态
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 获取：状态
     */
    public String getStatus() {
        return status;
    }
}
