package com.platform.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 实体
 * 表名 purchase_goods_main
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-02-19 09:02:41
 */
public class PurchaseGoodsMainEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    //
    private Long keyId;
    //代码
    private String productId;
    //度量单位
    private String productMeasurement;
    //物品名称
    private String productName;
    //物品详细描述
    private String productDescription;
    //物品备注
    private String productMemo;
    //规格
    private String productClass;
    //状态
    private String status;
    //规格型号
    private String guige;
    //代码
    private String daima;

    public String getGuige() {
        return guige;
    }

    public void setGuige(String guige) {
        this.guige = guige;
    }

    public String getDaima() {
        return daima;
    }

    public void setDaima(String daima) {
        this.daima = daima;
    }

    /**
     * 设置：
     */
    public void setKeyId(Long keyId) {
        this.keyId = keyId;
    }

    /**
     * 获取：
     */
    public Long getKeyId() {
        return keyId;
    }
    /**
     * 设置：代码
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     * 获取：代码
     */
    public String getProductId() {
        return productId;
    }
    /**
     * 设置：度量单位
     */
    public void setProductMeasurement(String productMeasurement) {
        this.productMeasurement = productMeasurement;
    }

    /**
     * 获取：度量单位
     */
    public String getProductMeasurement() {
        return productMeasurement;
    }
    /**
     * 设置：物品名称
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * 获取：物品名称
     */
    public String getProductName() {
        return productName;
    }
    /**
     * 设置：物品详细描述
     */
    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    /**
     * 获取：物品详细描述
     */
    public String getProductDescription() {
        return productDescription;
    }
    /**
     * 设置：物品备注
     */
    public void setProductMemo(String productMemo) {
        this.productMemo = productMemo;
    }

    /**
     * 获取：物品备注
     */
    public String getProductMemo() {
        return productMemo;
    }
    /**
     * 设置：规格
     */
    public void setProductClass(String productClass) {
        this.productClass = productClass;
    }

    /**
     * 获取：规格
     */
    public String getProductClass() {
        return productClass;
    }
    /**
     * 设置：状态
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 获取：状态
     */
    public String getStatus() {
        return status;
    }
}
