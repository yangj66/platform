package com.platform.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.platform.dao.ChauffeurDao;
import com.platform.entity.ChauffeurEntity;
import com.platform.service.ChauffeurService;

/**
 * Service实现类
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-04-25 10:48:03
 */
@Service("chauffeurService")
public class ChauffeurServiceImpl implements ChauffeurService {
    @Autowired
    private ChauffeurDao chauffeurDao;

    @Override
    public ChauffeurEntity queryObject(Integer keyId) {
        return chauffeurDao.queryObject(keyId);
    }

    @Override
    public List<ChauffeurEntity> queryList(Map<String, Object> map) {
        return chauffeurDao.queryList(map);
    }

    @Override
    public int queryTotal(Map<String, Object> map) {
        return chauffeurDao.queryTotal(map);
    }

    @Override
    public int save(ChauffeurEntity chauffeur) {
        return chauffeurDao.save(chauffeur);
    }

    @Override
    public int update(ChauffeurEntity chauffeur) {
        return chauffeurDao.update(chauffeur);
    }

    @Override
    public int delete(Integer keyId) {
        return chauffeurDao.delete(keyId);
    }

    @Override
    public int deleteBatch(Integer[] keyIds) {
        return chauffeurDao.deleteBatch(keyIds);
    }
}
