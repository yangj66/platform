package com.platform.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.platform.dao.FinanceAuxiliaryAccountingDao;
import com.platform.entity.FinanceAuxiliaryAccountingEntity;
import com.platform.service.FinanceAuxiliaryAccountingService;

/**
 * Service实现类
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2018-07-02 16:23:52
 */
@Service("financeAuxiliaryAccountingService")
public class FinanceAuxiliaryAccountingServiceImpl implements FinanceAuxiliaryAccountingService {
    @Autowired
    private FinanceAuxiliaryAccountingDao financeAuxiliaryAccountingDao;

    @Override
    public FinanceAuxiliaryAccountingEntity queryObject(Integer id) {
        return financeAuxiliaryAccountingDao.queryObject(id);
    }

    @Override
    public List<FinanceAuxiliaryAccountingEntity> queryList(Map<String, Object> map) {
        return financeAuxiliaryAccountingDao.queryList(map);
    }

    @Override
    public int queryTotal(Map<String, Object> map) {
        return financeAuxiliaryAccountingDao.queryTotal(map);
    }

    @Override
    public int save(FinanceAuxiliaryAccountingEntity financeAuxiliaryAccounting) {
        return financeAuxiliaryAccountingDao.save(financeAuxiliaryAccounting);
    }

    @Override
    public int update(FinanceAuxiliaryAccountingEntity financeAuxiliaryAccounting) {
        return financeAuxiliaryAccountingDao.update(financeAuxiliaryAccounting);
    }

    @Override
    public int delete(Integer id) {
        return financeAuxiliaryAccountingDao.delete(id);
    }

    @Override
    public int deleteBatch(Integer[]ids) {
        return financeAuxiliaryAccountingDao.deleteBatch(ids);
    }
}
