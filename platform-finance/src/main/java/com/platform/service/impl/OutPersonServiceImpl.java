package com.platform.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.platform.dao.OutPersonDao;
import com.platform.entity.OutPersonEntity;
import com.platform.service.OutPersonService;

/**
 * Service实现类
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-04-25 10:48:03
 */
@Service("outPersonService")
public class OutPersonServiceImpl implements OutPersonService {
    @Autowired
    private OutPersonDao outPersonDao;

    @Override
    public OutPersonEntity queryObject(Integer keyId) {
        return outPersonDao.queryObject(keyId);
    }

    @Override
    public List<OutPersonEntity> queryList(Map<String, Object> map) {
        return outPersonDao.queryList(map);
    }

    @Override
    public int queryTotal(Map<String, Object> map) {
        return outPersonDao.queryTotal(map);
    }

    @Override
    public int save(OutPersonEntity outPerson) {
        return outPersonDao.save(outPerson);
    }

    @Override
    public int update(OutPersonEntity outPerson) {
        return outPersonDao.update(outPerson);
    }

    @Override
    public int delete(Integer keyId) {
        return outPersonDao.delete(keyId);
    }

    @Override
    public int deleteBatch(Integer[] keyIds) {
        return outPersonDao.deleteBatch(keyIds);
    }
}
