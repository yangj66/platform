package com.platform.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.platform.dao.PurchaseHardnessDao;
import com.platform.entity.PurchaseHardnessEntity;
import com.platform.service.PurchaseHardnessService;

/**
 * Service实现类
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-02-18 21:26:57
 */
@Service("purchaseHardnessService")
public class PurchaseHardnessServiceImpl implements PurchaseHardnessService {
    @Autowired
    private PurchaseHardnessDao purchaseHardnessDao;

    @Override
    public PurchaseHardnessEntity queryObject(Integer id) {
        return purchaseHardnessDao.queryObject(id);
    }

    @Override
    public List<PurchaseHardnessEntity> queryList(Map<String, Object> map) {
        return purchaseHardnessDao.queryList(map);
    }

    @Override
    public int queryTotal(Map<String, Object> map) {
        return purchaseHardnessDao.queryTotal(map);
    }

    @Override
    public int save(PurchaseHardnessEntity purchaseHardness) {
        return purchaseHardnessDao.save(purchaseHardness);
    }

    @Override
    public int update(PurchaseHardnessEntity purchaseHardness) {
        return purchaseHardnessDao.update(purchaseHardness);
    }

    @Override
    public int delete(Integer id) {
        return purchaseHardnessDao.delete(id);
    }

    @Override
    public int deleteBatch(Integer[] ids) {
        return purchaseHardnessDao.deleteBatch(ids);
    }
}
