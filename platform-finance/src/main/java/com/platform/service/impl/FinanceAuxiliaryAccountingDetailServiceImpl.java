package com.platform.service.impl;

import com.platform.utils.RRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.platform.dao.FinanceAuxiliaryAccountingDetailDao;
import com.platform.entity.FinanceAuxiliaryAccountingDetailEntity;
import com.platform.service.FinanceAuxiliaryAccountingDetailService;

/**
 * Service实现类
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2018-07-02 10:21:17
 */
@Service("financeAuxiliaryAccountingDetailService")
public class FinanceAuxiliaryAccountingDetailServiceImpl implements FinanceAuxiliaryAccountingDetailService {
    @Autowired
    private FinanceAuxiliaryAccountingDetailDao financeAuxiliaryAccountingDetailDao;

    @Override
    public FinanceAuxiliaryAccountingDetailEntity queryObject(Integer id) {
        return financeAuxiliaryAccountingDetailDao.queryObject(id);
    }

    @Override
    public List<FinanceAuxiliaryAccountingDetailEntity> queryList(Map<String, Object> map) {
        return financeAuxiliaryAccountingDetailDao.queryList(map);
    }

    @Override
    public int queryTotal(Map<String, Object> map) {
        return financeAuxiliaryAccountingDetailDao.queryTotal(map);
    }

    @Override
    public int save(FinanceAuxiliaryAccountingDetailEntity financeAuxiliaryAccountingDetail) {
        return financeAuxiliaryAccountingDetailDao.save(financeAuxiliaryAccountingDetail);
    }

    @Override
    public int update(FinanceAuxiliaryAccountingDetailEntity financeAuxiliaryAccountingDetail) {
        return financeAuxiliaryAccountingDetailDao.update(financeAuxiliaryAccountingDetail);
    }

    @Override
    public int delete(Integer id) {
        return financeAuxiliaryAccountingDetailDao.delete(id);
    }

    @Override
    public int deleteBatch(Integer[]ids) {
        return financeAuxiliaryAccountingDetailDao.deleteBatch(ids);
    }

    /**
     * 启用
     *
     * @param id
     * @return
     */
    @Override
    public int enUse(Integer id) {
        FinanceAuxiliaryAccountingDetailEntity financeAuxiliaryAccountingDetailEntity = queryObject(id);
        if ("on".equals(financeAuxiliaryAccountingDetailEntity.getUseFlag())) {
            throw new RRException("此项已处于启用状态！");
        }
        financeAuxiliaryAccountingDetailEntity.setUseFlag("on");
//        financeAuxiliaryAccountingDetailEntity.setUpdateUserId(user.getUserId());
//        financeAuxiliaryAccountingDetailEntity.setUpdateTime(new Date());
        return financeAuxiliaryAccountingDetailDao.update(financeAuxiliaryAccountingDetailEntity);
    }

    /**
     * 禁用
     *
     * @param id
     * @return
     */
    @Override
    public int unUse(Integer id) {
        FinanceAuxiliaryAccountingDetailEntity financeAuxiliaryAccountingDetailEntity = queryObject(id);
        if ("off".equals(financeAuxiliaryAccountingDetailEntity.getUseFlag())) {
            throw new RRException("此项已处于禁用状态！");
        }
        financeAuxiliaryAccountingDetailEntity.setUseFlag("off");
//        financeAuxiliaryAccountingDetailEntity.setUpdateUserId(user.getUserId());
//        financeAuxiliaryAccountingDetailEntity.setUpdateTime(new Date());
        return financeAuxiliaryAccountingDetailDao.update(financeAuxiliaryAccountingDetailEntity);
    }
}
