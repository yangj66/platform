package com.platform.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.platform.dao.CarIdDao;
import com.platform.entity.CarIdEntity;
import com.platform.service.CarIdService;

/**
 * Service实现类
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-04-25 10:48:02
 */
@Service("carIdService")
public class CarIdServiceImpl implements CarIdService {
    @Autowired
    private CarIdDao carIdDao;

    @Override
    public CarIdEntity queryObject(Integer keyId) {
        return carIdDao.queryObject(keyId);
    }

    @Override
    public List<CarIdEntity> queryList(Map<String, Object> map) {
        return carIdDao.queryList(map);
    }

    @Override
    public int queryTotal(Map<String, Object> map) {
        return carIdDao.queryTotal(map);
    }

    @Override
    public int save(CarIdEntity carId) {
        return carIdDao.save(carId);
    }

    @Override
    public int update(CarIdEntity carId) {
        return carIdDao.update(carId);
    }

    @Override
    public int delete(Integer keyId) {
        return carIdDao.delete(keyId);
    }

    @Override
    public int deleteBatch(Integer[] keyIds) {
        return carIdDao.deleteBatch(keyIds);
    }
}
