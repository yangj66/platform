package com.platform.service.impl;

import com.platform.entity.OrDeleteLogEntity;
import com.platform.service.OrDeleteLogService;
import com.platform.utils.RRException;
import com.platform.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.platform.dao.PurchaseGoodsStockInDao;
import com.platform.entity.PurchaseGoodsStockInEntity;
import com.platform.service.PurchaseGoodsStockInService;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service实现类
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-02-12 13:25:53
 */
@Service("purchaseGoodsStockInService")
public class PurchaseGoodsStockInServiceImpl implements PurchaseGoodsStockInService {
    @Autowired
    private PurchaseGoodsStockInDao purchaseGoodsStockInDao;
    @Autowired
    private OrDeleteLogService orDeleteLogService;

    @Override
    public PurchaseGoodsStockInEntity queryObject(Long keyId) {
        return purchaseGoodsStockInDao.queryObject(keyId);
    }

    @Override
    public List<PurchaseGoodsStockInEntity> queryList(Map<String, Object> map) {
        return purchaseGoodsStockInDao.queryList(map);
    }

    @Override
    public int queryTotal(Map<String, Object> map) {
        return purchaseGoodsStockInDao.queryTotal(map);
    }

    @Override
    public int save(PurchaseGoodsStockInEntity purchaseGoodsStockIn) {
        return purchaseGoodsStockInDao.save(purchaseGoodsStockIn);
    }

    @Override
    public void saveMap(Map<String, Object> map){
        purchaseGoodsStockInDao.saveMap(map);
    }

    @Override
    public int update(PurchaseGoodsStockInEntity purchaseGoodsStockIn) {
        return purchaseGoodsStockInDao.update(purchaseGoodsStockIn);
    }

    @Override
    public int delete(Long keyId) {
        return purchaseGoodsStockInDao.delete(keyId);
    }

    @Override
    @Transactional
    public int deleteBatch(Long[] keyIds) {
        //先检查
        if(keyIds!=null && keyIds.length>0) {
            Date now = new Date();
            for (int i = 0; i < keyIds.length; i++) {
                Long keyId = keyIds[i];
                PurchaseGoodsStockInEntity stockIn = this.queryObject(keyId.longValue());
                if (!"0".equals(stockIn.getStatus()) || stockIn.getOutQuantity().doubleValue() > 0.0) {
                    throw new RRException("所选记录已出库，请先冲出库单，再进行删除");
                }

                //记录删除日志
                OrDeleteLogEntity deleteLogEntity = new OrDeleteLogEntity();
                deleteLogEntity.setType("删除入库记录");
                deleteLogEntity.setOperateTime(now);
                deleteLogEntity.setOrderId(stockIn.getStockInId());
                deleteLogEntity.setUserId(ShiroUtils.getUserId());
                StringBuilder str = new StringBuilder();
                str.append("删除记录的详细信息:");
                str.append("--入库编号:");
                str.append(stockIn.getStockId());
                str.append("--物品ID:");
                str.append(stockIn.getProductId());
                str.append("--入库数量:");
                str.append(stockIn.getInQuantity());
                str.append("--录入时间:");
                str.append(stockIn.getInputDate());
                str.append("--入库时间:");
                str.append(stockIn.getStockInDate());
                str.append("--入库人:");
                str.append(stockIn.getStockInResponsier());
                str.append("--仓库ID:");
                str.append(stockIn.getStockInId());
                str.append("--供应商ID:");
                str.append(stockIn.getSupplierId());
                str.append("--条目号:");
                str.append(stockIn.getItemNumber());
                deleteLogEntity.setMemo(str.toString());
                orDeleteLogService.add(deleteLogEntity);
            }
            return purchaseGoodsStockInDao.deleteBatch(keyIds);
        }else
            return 0;
    }

    @Override
    @Transactional
    public void deleteByStockInId(String stockInId)  {

        List<PurchaseGoodsStockInEntity> list = purchaseGoodsStockInDao.queryList(new HashMap<String,Object>().put("stockInId",stockInId));
        if(list != null && list.size()>0)
        {
            Date now = new Date();
            for(PurchaseGoodsStockInEntity stockIn: list)
            {
                //记录删除日志
                OrDeleteLogEntity deleteLogEntity = new OrDeleteLogEntity();
                deleteLogEntity.setType("修改入库记录(先删除再新增)");
                deleteLogEntity.setOperateTime(now);
                deleteLogEntity.setOrderId(stockIn.getStockInId());
                deleteLogEntity.setUserId(ShiroUtils.getUserId());
                StringBuilder str = new StringBuilder();
                str.append("删除记录的详细信息:");
                str.append("--入库编号:");
                str.append(stockIn.getStockId());
                str.append("--物品ID:");
                str.append(stockIn.getProductId());
                str.append("--入库数量:");
                str.append(stockIn.getInQuantity());
                str.append("--录入时间:");
                str.append(stockIn.getInputDate());
                str.append("--入库时间:");
                str.append(stockIn.getStockInDate());
                str.append("--入库人:");
                str.append(stockIn.getStockInResponsier());
                str.append("--仓库ID:");
                str.append(stockIn.getStockInId());
                str.append("--供应商ID:");
                str.append(stockIn.getSupplierId());
                str.append("--条目号:");
                str.append(stockIn.getItemNumber());
                deleteLogEntity.setMemo(str.toString());
                orDeleteLogService.add(deleteLogEntity);
            }
        }

        purchaseGoodsStockInDao.deleteByStockInId(stockInId);
    }

    @Override
    public String getMaxId(String prefix) {
        return purchaseGoodsStockInDao.getMaxId(prefix);
    }
}
