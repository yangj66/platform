package com.platform.service.impl;

import com.platform.entity.OrDeleteLogEntity;
import com.platform.entity.PurchaseGoodsStockInEntity;
import com.platform.service.OrDeleteLogService;
import com.platform.service.PurchaseGoodsStockInService;
import com.platform.utils.RRException;
import com.platform.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.platform.dao.PurchaseGoodsStockOutDao;
import com.platform.entity.PurchaseGoodsStockOutEntity;
import com.platform.service.PurchaseGoodsStockOutService;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service实现类
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-02-18 21:26:57
 */
@Service("purchaseGoodsStockOutService")
public class PurchaseGoodsStockOutServiceImpl implements PurchaseGoodsStockOutService {
    @Autowired
    private PurchaseGoodsStockOutDao purchaseGoodsStockOutDao;
    @Autowired
    private PurchaseGoodsStockInService purchaseGoodsStockInService;
    @Autowired
    private OrDeleteLogService orDeleteLogService;

    @Override
    public PurchaseGoodsStockOutEntity queryObject(Long keyId) {
        return purchaseGoodsStockOutDao.queryObject(keyId);
    }

    @Override
    public List<PurchaseGoodsStockOutEntity> queryList(Map<String, Object> map) {
        return purchaseGoodsStockOutDao.queryList(map);
    }

    @Override
    public int queryTotal(Map<String, Object> map) {
        return purchaseGoodsStockOutDao.queryTotal(map);
    }

    @Override
    public int save(PurchaseGoodsStockOutEntity purchaseGoodsStockOut) {
        return purchaseGoodsStockOutDao.save(purchaseGoodsStockOut);
    }

    @Override
    public int update(PurchaseGoodsStockOutEntity purchaseGoodsStockOut) {
        return purchaseGoodsStockOutDao.update(purchaseGoodsStockOut);
    }

    @Override
    public int delete(Long keyId) {
        return purchaseGoodsStockOutDao.delete(keyId);
    }

    @Override
    public int deleteBatch(Long[] keyIds) {
        return purchaseGoodsStockOutDao.deleteBatch(keyIds);
    }

    @Override
    @Transactional
    public void saveMap(Map<String, Object> map){
        List inList = (List)map.get("inList");
        //先处理入库表数据
        for(int i=0; i<inList.size(); i++)
        {
            Map item  = (Map)inList.get(i);
            Integer keyId = (Integer)item.get("keyId");
            Boolean checked = (Boolean)item.get("checked");

            if(checked) {//只处理选中想
                Double stockOutQuantity = 0.0;
                if (item.get("stockOutQuantity") instanceof Integer) {
                    Integer qty = (Integer) item.get("stockOutQuantity");
                    stockOutQuantity = qty.doubleValue();
                } else if (item.get("stockOutQuantity") instanceof Double) {
                    stockOutQuantity = (Double) item.get("stockOutQuantity");
                }
                PurchaseGoodsStockInEntity stockIn = purchaseGoodsStockInService.queryObject(keyId.longValue());
                if(stockIn.getStockQuantity().compareTo(stockOutQuantity)<0)
                {
                    throw new RRException("库存数量不足，不能出库！");
                }
                stockIn.setStockQuantity(stockIn.getStockQuantity() - stockOutQuantity);
                stockIn.setOutQuantity(stockIn.getOutQuantity() + stockOutQuantity);
                if (stockIn.getStockQuantity().doubleValue() != 0.0) {
                    stockIn.setStatus("1");//出库中
                } else {
                    stockIn.setStatus("2");//已出完
                }
                purchaseGoodsStockInService.update(stockIn);
                System.out.println(checked.toString() + "====checked===="+i);
            }else{
                //inList.remove(i); //未选中的从inList中删除
                System.out.println(checked.toString() + "====nochecked===="+i);
            }
        }

        //map.put("inList",inList);//处理好的inList重新放入map

        purchaseGoodsStockOutDao.saveMap(map);
    }


    @Override
    @Transactional
    public int undoBatch(Long[] keyIds) {
        //先检查
        Date now = new Date();
        for(int i=0; i<keyIds.length; i++) {
            Long keyId = keyIds[i];
            PurchaseGoodsStockOutEntity stockOut = this.queryObject(keyId.longValue());
            if(stockOut.getWithdrawFlag().intValue()!=0)
            {
                throw new RRException("已冲单的出库单，不能重复冲单！");
            }

            //记录删除日志
            OrDeleteLogEntity deleteLogEntity = new OrDeleteLogEntity();
            deleteLogEntity.setType("删除出库记录");
            deleteLogEntity.setOperateTime(now);
            deleteLogEntity.setOrderId(stockOut.getStockInId());
            deleteLogEntity.setUserId(ShiroUtils.getUserId());
            StringBuilder str = new StringBuilder();
            str.append("删除记录的详细信息:");
            str.append("--出库编号:");
            str.append(stockOut.getStockOutId());
            str.append("--入库编号:");
            str.append(stockOut.getStockId());
            str.append("--入库keyId:");
            str.append(stockOut.getStockInKeyId());
            str.append("--物品ID:");
            str.append(stockOut.getProductId());
            str.append("--出库数量:");
            str.append(stockOut.getStockOutQuantity());
            str.append("--净重:");
            str.append(stockOut.getNetWeight());
            str.append("--皮重:");
            str.append(stockOut.getTareWeight());
            str.append("--毛重:");
            str.append(stockOut.getGrossWeight());

            str.append("--录入时间:");
            str.append(stockOut.getInputDate());
            str.append("--出库时间:");
            str.append(stockOut.getStockOutDate());
            str.append("--出库责任人:");
            str.append(stockOut.getStockOutResponsier());
            str.append("--仓库ID:");
            str.append(stockOut.getStockInId());
            str.append("--供应商ID:");
            str.append(stockOut.getSupplierId());
            str.append("--条目号:");
            str.append(stockOut.getStockInItemNumber());
            str.append("--联系电话:");
            str.append(stockOut.getTelephone());
            str.append("--联系人:");
            str.append(stockOut.getContacter());
            str.append("--承运:");
            str.append(stockOut.getCarId());
            str.append("--司机:");
            str.append(stockOut.getChauffeur());
            str.append("--送货地址:");
            str.append(stockOut.getAddress());
            str.append("--发货人:");
            str.append(stockOut.getOutPerson());

            deleteLogEntity.setMemo(str.toString());
            orDeleteLogService.add(deleteLogEntity);
        }
        //先处理入库表数据
        for(int i=0; i<keyIds.length; i++)
        {
            Long keyId = keyIds[i];
            PurchaseGoodsStockOutEntity stockOut = this.queryObject(keyId.longValue());
            PurchaseGoodsStockInEntity stockIn = purchaseGoodsStockInService.queryObject(stockOut.getStockInKeyId());

            stockIn.setStockQuantity(stockIn.getStockQuantity() + stockOut.getStockOutQuantity());
            stockIn.setOutQuantity(stockIn.getOutQuantity() - stockOut.getStockOutQuantity());
            if (stockIn.getStockQuantity().doubleValue() == stockIn.getInQuantity().doubleValue()) {
                stockIn.setStatus("0");//未出库
            }else if (stockIn.getStockQuantity().doubleValue() != 0.0) {
                stockIn.setStatus("1");//出库中
            }else {
                stockIn.setStatus("2");//已出完
            }
            purchaseGoodsStockInService.update(stockIn);
        }
        //return purchaseGoodsStockOutDao.undoBatch(keyIds);
        return purchaseGoodsStockOutDao.deleteBatch(keyIds);
    }


    public void count(Long[] keyIds, Double unitPrice)
    {
        purchaseGoodsStockOutDao.count(keyIds, unitPrice);
    }

    @Override
    public String getMaxId(String prefix) {
        return purchaseGoodsStockOutDao.getMaxId(prefix);
    }
}
