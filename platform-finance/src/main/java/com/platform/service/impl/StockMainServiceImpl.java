package com.platform.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.platform.dao.StockMainDao;
import com.platform.entity.StockMainEntity;
import com.platform.service.StockMainService;

/**
 * Service实现类
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-02-12 11:58:53
 */
@Service("stockMainService")
public class StockMainServiceImpl implements StockMainService {
    @Autowired
    private StockMainDao stockMainDao;

    @Override
    public StockMainEntity queryObject(Long keyId) {
        return stockMainDao.queryObject(keyId);
    }

    @Override
    public List<StockMainEntity> queryList(Map<String, Object> map) {
        return stockMainDao.queryList(map);
    }

    @Override
    public int queryTotal(Map<String, Object> map) {
        return stockMainDao.queryTotal(map);
    }

    @Override
    public int save(StockMainEntity stockMain) {
        return stockMainDao.save(stockMain);
    }

    @Override
    public int update(StockMainEntity stockMain) {
        return stockMainDao.update(stockMain);
    }

    @Override
    public int delete(Long keyId) {
        return stockMainDao.delete(keyId);
    }

    @Override
    public int deleteBatch(Long[] keyIds) {
        return stockMainDao.deleteBatch(keyIds);
    }
}
