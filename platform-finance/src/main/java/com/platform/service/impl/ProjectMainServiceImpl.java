package com.platform.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.platform.dao.ProjectMainDao;
import com.platform.entity.ProjectMainEntity;
import com.platform.service.ProjectMainService;

/**
 * Service实现类
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-03-18 10:21:37
 */
@Service("projectMainService")
public class ProjectMainServiceImpl implements ProjectMainService {
    @Autowired
    private ProjectMainDao projectMainDao;

    @Override
    public ProjectMainEntity queryObject(Long keyId) {
        return projectMainDao.queryObject(keyId);
    }

    @Override
    public List<ProjectMainEntity> queryList(Map<String, Object> map) {
        return projectMainDao.queryList(map);
    }

    @Override
    public int queryTotal(Map<String, Object> map) {
        return projectMainDao.queryTotal(map);
    }

    @Override
    public int save(ProjectMainEntity projectMain) {
        return projectMainDao.save(projectMain);
    }

    @Override
    public int update(ProjectMainEntity projectMain) {
        return projectMainDao.update(projectMain);
    }

    @Override
    public int delete(Long keyId) {
        return projectMainDao.delete(keyId);
    }

    @Override
    public int deleteBatch(Long[] keyIds) {
        return projectMainDao.deleteBatch(keyIds);
    }
}
