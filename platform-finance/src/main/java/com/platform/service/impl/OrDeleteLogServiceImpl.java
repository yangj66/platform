/*
 * 类名称:OrDeleteLogServiceImpl.java
 * 包名称:com.platform.service.impl
 *
 * 修改履历:
 *     日期                       修正者        主要内容
 *     2019-10-09 09:10:55        lipengjun     初版做成
 *
 * Copyright (c) 2019-2019 微同软件
 */
package com.platform.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.platform.utils.QueryPlus;
import com.platform.dao.OrDeleteLogDao;
import com.platform.entity.OrDeleteLogEntity;
import com.platform.service.OrDeleteLogService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Service实现类
 *
 * @author lipengjun
 * @date 2019-10-09 09:10:55
 */
@Service("orDeleteLogService")
public class OrDeleteLogServiceImpl extends ServiceImpl<OrDeleteLogDao, OrDeleteLogEntity> implements OrDeleteLogService {

    @Override
    public List<OrDeleteLogEntity> queryAll(Map<String, Object> params) {
        return baseMapper.queryAll(params);
    }

    @Override
    public Page queryPage(Map<String, Object> params) {
        //排序
        params.put("sidx", "T.id");
        params.put("asc", false);
        Page<OrDeleteLogEntity> page = new QueryPlus<OrDeleteLogEntity>(params).getPage();
        return page.setRecords(baseMapper.selectOrDeleteLogPage(page, params));
    }

    @Override
    public boolean add(OrDeleteLogEntity orDeleteLog) {
        return this.save(orDeleteLog);
    }

    @Override
    public boolean update(OrDeleteLogEntity orDeleteLog) {
        return this.updateById(orDeleteLog);
    }

    @Override
    public boolean delete(Long id) {
        return this.removeById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteBatch(Long[] ids) {
        return this.removeByIds(Arrays.asList(ids));
    }
}
