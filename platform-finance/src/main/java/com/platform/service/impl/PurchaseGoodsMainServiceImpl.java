package com.platform.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.platform.dao.PurchaseGoodsMainDao;
import com.platform.entity.PurchaseGoodsMainEntity;
import com.platform.service.PurchaseGoodsMainService;

/**
 * Service实现类
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-02-19 09:02:41
 */
@Service("purchaseGoodsMainService")
public class PurchaseGoodsMainServiceImpl implements PurchaseGoodsMainService {
    @Autowired
    private PurchaseGoodsMainDao purchaseGoodsMainDao;

    @Override
    public PurchaseGoodsMainEntity queryObject(Long keyId) {
        return purchaseGoodsMainDao.queryObject(keyId);
    }

    @Override
    public List<PurchaseGoodsMainEntity> queryList(Map<String, Object> map) {
        return purchaseGoodsMainDao.queryList(map);
    }

    @Override
    public int queryTotal(Map<String, Object> map) {
        return purchaseGoodsMainDao.queryTotal(map);
    }

    @Override
    public int save(PurchaseGoodsMainEntity purchaseGoodsMain) {
        return purchaseGoodsMainDao.save(purchaseGoodsMain);
    }

    @Override
    public int update(PurchaseGoodsMainEntity purchaseGoodsMain) {
        return purchaseGoodsMainDao.update(purchaseGoodsMain);
    }

    @Override
    public int delete(Long keyId) {
        return purchaseGoodsMainDao.delete(keyId);
    }

    @Override
    public int deleteBatch(Long[] keyIds) {
        return purchaseGoodsMainDao.deleteBatch(keyIds);
    }
}
