package com.platform.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.platform.dao.SupplierMainDao;
import com.platform.entity.SupplierMainEntity;
import com.platform.service.SupplierMainService;

/**
 * Service实现类
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-02-15 15:15:32
 */
@Service("supplierMainService")
public class SupplierMainServiceImpl implements SupplierMainService {
    @Autowired
    private SupplierMainDao supplierMainDao;

    @Override
    public SupplierMainEntity queryObject(Long keyId) {
        return supplierMainDao.queryObject(keyId);
    }

    @Override
    public List<SupplierMainEntity> queryList(Map<String, Object> map) {
        return supplierMainDao.queryList(map);
    }

    @Override
    public int queryTotal(Map<String, Object> map) {
        return supplierMainDao.queryTotal(map);
    }

    @Override
    public int save(SupplierMainEntity supplierMain) {
        return supplierMainDao.save(supplierMain);
    }

    @Override
    public int update(SupplierMainEntity supplierMain) {
        return supplierMainDao.update(supplierMain);
    }

    @Override
    public int delete(Long keyId) {
        return supplierMainDao.delete(keyId);
    }

    @Override
    public int deleteBatch(Long[] keyIds) {
        return supplierMainDao.deleteBatch(keyIds);
    }
}
