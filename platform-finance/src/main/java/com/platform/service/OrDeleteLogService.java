/*
 * 类名称:OrDeleteLogService.java
 * 包名称:com.platform.service
 *
 * 修改履历:
 *     日期                       修正者        主要内容
 *     2019-10-09 09:10:55        lipengjun     初版做成
 *
 * Copyright (c) 2019-2019 微同软件
 */
package com.platform.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.platform.entity.OrDeleteLogEntity;

import java.util.List;
import java.util.Map;

/**
 * Service接口
 *
 * @author lipengjun
 * @date 2019-10-09 09:10:55
 */
public interface OrDeleteLogService extends IService<OrDeleteLogEntity> {

    /**
     * 查询所有列表
     *
     * @param params 查询参数
     * @return List
     */
    List<OrDeleteLogEntity> queryAll(Map<String, Object> params);

    /**
     * 分页查询
     *
     * @param params 查询参数
     * @return Page
     */
    Page queryPage(Map<String, Object> params);

    /**
     * 新增
     *
     * @param orDeleteLog 
     * @return 新增结果
     */
    boolean add(OrDeleteLogEntity orDeleteLog);

    /**
     * 根据主键更新
     *
     * @param orDeleteLog 
     * @return 更新结果
     */
    boolean update(OrDeleteLogEntity orDeleteLog);

    /**
     * 根据主键删除
     *
     * @param id id
     * @return 删除结果
     */
    boolean delete(Long id);

    /**
     * 根据主键批量删除
     *
     * @param ids ids
     * @return 删除结果
     */
    boolean deleteBatch(Long[] ids);
}
