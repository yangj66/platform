package com.platform.service;

import com.platform.entity.ChauffeurEntity;

import java.util.List;
import java.util.Map;

/**
 * Service接口
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-04-25 10:48:03
 */
public interface ChauffeurService {

    /**
     * 根据主键查询实体
     *
     * @param id 主键
     * @return 实体
     */
    ChauffeurEntity queryObject(Integer keyId);

    /**
     * 分页查询
     *
     * @param map 参数
     * @return list
     */
    List<ChauffeurEntity> queryList(Map<String, Object> map);

    /**
     * 分页统计总数
     *
     * @param map 参数
     * @return 总数
     */
    int queryTotal(Map<String, Object> map);

    /**
     * 保存实体
     *
     * @param chauffeur 实体
     * @return 保存条数
     */
    int save(ChauffeurEntity chauffeur);

    /**
     * 根据主键更新实体
     *
     * @param chauffeur 实体
     * @return 更新条数
     */
    int update(ChauffeurEntity chauffeur);

    /**
     * 根据主键删除
     *
     * @param keyId
     * @return 删除条数
     */
    int delete(Integer keyId);

    /**
     * 根据主键批量删除
     *
     * @param keyIds
     * @return 删除条数
     */
    int deleteBatch(Integer[] keyIds);
}
