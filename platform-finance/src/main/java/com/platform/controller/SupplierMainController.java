package com.platform.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.platform.entity.SupplierMainEntity;
import com.platform.service.SupplierMainService;
import com.platform.utils.PageUtils;
import com.platform.utils.Query;
import com.platform.utils.R;

/**
 * Controller
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-02-15 15:15:32
 */
@RestController
@RequestMapping("suppliermain")
public class SupplierMainController {
    @Autowired
    private SupplierMainService supplierMainService;

    /**
     * 查看列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("suppliermain:list")
    public R list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);

        List<SupplierMainEntity> supplierMainList = supplierMainService.queryList(query);
        int total = supplierMainService.queryTotal(query);

        PageUtils pageUtil = new PageUtils(supplierMainList, total, query.getLimit(), query.getPage());

        return R.ok().put("page", pageUtil);
    }

    /**
     * 查看信息
     */
    @RequestMapping("/info/{keyId}")
    @RequiresPermissions("suppliermain:info")
    public R info(@PathVariable("keyId") Long keyId) {
        SupplierMainEntity supplierMain = supplierMainService.queryObject(keyId);

        return R.ok().put("supplierMain", supplierMain);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("suppliermain:save")
    public R save(@RequestBody SupplierMainEntity supplierMain) {
        supplierMainService.save(supplierMain);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("suppliermain:update")
    public R update(@RequestBody SupplierMainEntity supplierMain) {
        supplierMainService.update(supplierMain);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("suppliermain:delete")
    public R delete(@RequestBody Long[] keyIds) {
        supplierMainService.deleteBatch(keyIds);

        return R.ok();
    }

    /**
     * 查看所有列表
     */
    @RequestMapping("/queryAll")
    public R queryAll(@RequestParam Map<String, Object> params) {

        List<SupplierMainEntity> list = supplierMainService.queryList(params);

        return R.ok().put("list", list);
    }
}
