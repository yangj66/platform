package com.platform.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.platform.entity.PurchaseGoodsMainEntity;
import com.platform.service.PurchaseGoodsMainService;
import com.platform.utils.PageUtils;
import com.platform.utils.Query;
import com.platform.utils.R;

/**
 * Controller
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-02-19 09:02:41
 */
@RestController
@RequestMapping("purchasegoodsmain")
public class PurchaseGoodsMainController {
    @Autowired
    private PurchaseGoodsMainService purchaseGoodsMainService;

    /**
     * 查看列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("purchasegoodsmain:list")
    public R list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);

        List<PurchaseGoodsMainEntity> purchaseGoodsMainList = purchaseGoodsMainService.queryList(query);
        int total = purchaseGoodsMainService.queryTotal(query);

        PageUtils pageUtil = new PageUtils(purchaseGoodsMainList, total, query.getLimit(), query.getPage());

        return R.ok().put("page", pageUtil);
    }

    /**
     * 查看信息
     */
    @RequestMapping("/info/{keyId}")
    @RequiresPermissions("purchasegoodsmain:info")
    public R info(@PathVariable("keyId") Long keyId) {
        PurchaseGoodsMainEntity purchaseGoodsMain = purchaseGoodsMainService.queryObject(keyId);

        return R.ok().put("purchaseGoodsMain", purchaseGoodsMain);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("purchasegoodsmain:save")
    public R save(@RequestBody PurchaseGoodsMainEntity purchaseGoodsMain) {
        Map params = new HashMap();
        params.put("productId",purchaseGoodsMain.getProductId());
        int count = purchaseGoodsMainService.queryTotal(params);
        if(count>0)
        {
           return R.error("已有编码"+purchaseGoodsMain.getProductId()+",不能重复！");
        }
        purchaseGoodsMainService.save(purchaseGoodsMain);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("purchasegoodsmain:update")
    public R update(@RequestBody PurchaseGoodsMainEntity purchaseGoodsMain) {
        purchaseGoodsMainService.update(purchaseGoodsMain);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("purchasegoodsmain:delete")
    public R delete(@RequestBody Long[] keyIds) {
        purchaseGoodsMainService.deleteBatch(keyIds);

        return R.ok();
    }

    /**
     * 查看所有列表
     */
    @RequestMapping("/queryAll")
    public R queryAll(@RequestParam Map<String, Object> params) {

        List<PurchaseGoodsMainEntity> list = purchaseGoodsMainService.queryList(params);

        return R.ok().put("list", list);
    }
}
