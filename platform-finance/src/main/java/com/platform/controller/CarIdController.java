package com.platform.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.platform.entity.CarIdEntity;
import com.platform.service.CarIdService;
import com.platform.utils.PageUtils;
import com.platform.utils.Query;
import com.platform.utils.R;

/**
 * Controller
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-04-25 10:48:02
 */
@RestController
@RequestMapping("carid")
public class CarIdController {
    @Autowired
    private CarIdService carIdService;

    /**
     * 查看列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("carid:list")
    public R list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);

        List<CarIdEntity> carIdList = carIdService.queryList(query);
        int total = carIdService.queryTotal(query);

        PageUtils pageUtil = new PageUtils(carIdList, total, query.getLimit(), query.getPage());

        return R.ok().put("page", pageUtil);
    }

    /**
     * 查看信息
     */
    @RequestMapping("/info/{keyId}")
    @RequiresPermissions("carid:info")
    public R info(@PathVariable("keyId") Integer keyId) {
        CarIdEntity carId = carIdService.queryObject(keyId);

        return R.ok().put("carId", carId);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("carid:save")
    public R save(@RequestBody CarIdEntity carId) {
        carIdService.save(carId);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("carid:update")
    public R update(@RequestBody CarIdEntity carId) {
        carIdService.update(carId);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("carid:delete")
    public R delete(@RequestBody Integer[] keyIds) {
        carIdService.deleteBatch(keyIds);

        return R.ok();
    }

    /**
     * 查看所有列表
     */
    @RequestMapping("/queryAll")
    public R queryAll(@RequestParam Map<String, Object> params) {

        List<CarIdEntity> list = carIdService.queryList(params);

        return R.ok().put("list", list);
    }
}
