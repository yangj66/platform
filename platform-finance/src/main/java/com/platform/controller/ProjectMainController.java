package com.platform.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.platform.entity.ProjectMainEntity;
import com.platform.service.ProjectMainService;
import com.platform.utils.PageUtils;
import com.platform.utils.Query;
import com.platform.utils.R;

/**
 * Controller
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-03-18 10:21:37
 */
@RestController
@RequestMapping("projectmain")
public class ProjectMainController {
    @Autowired
    private ProjectMainService projectMainService;

    /**
     * 查看列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("projectmain:list")
    public R list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);

        List<ProjectMainEntity> projectMainList = projectMainService.queryList(query);
        int total = projectMainService.queryTotal(query);

        PageUtils pageUtil = new PageUtils(projectMainList, total, query.getLimit(), query.getPage());

        return R.ok().put("page", pageUtil);
    }

    /**
     * 查看信息
     */
    @RequestMapping("/info/{keyId}")
    @RequiresPermissions("projectmain:info")
    public R info(@PathVariable("keyId") Long keyId) {
        ProjectMainEntity projectMain = projectMainService.queryObject(keyId);

        return R.ok().put("projectMain", projectMain);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("projectmain:save")
    public R save(@RequestBody ProjectMainEntity projectMain) {
        projectMainService.save(projectMain);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("projectmain:update")
    public R update(@RequestBody ProjectMainEntity projectMain) {
        projectMainService.update(projectMain);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("projectmain:delete")
    public R delete(@RequestBody Long[] keyIds) {
        projectMainService.deleteBatch(keyIds);

        return R.ok();
    }

    /**
     * 查看所有列表
     */
    @RequestMapping("/queryAll")
    public R queryAll(@RequestParam Map<String, Object> params) {

        List<ProjectMainEntity> list = projectMainService.queryList(params);

        return R.ok().put("list", list);
    }
}
