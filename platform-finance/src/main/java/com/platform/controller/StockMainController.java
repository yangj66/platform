package com.platform.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.platform.entity.StockMainEntity;
import com.platform.service.StockMainService;
import com.platform.utils.PageUtils;
import com.platform.utils.Query;
import com.platform.utils.R;

/**
 * Controller
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-02-12 11:58:53
 */
@RestController
@RequestMapping("stockmain")
public class StockMainController {
    @Autowired
    private StockMainService stockMainService;

    /**
     * 查看列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("stockmain:list")
    public R list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);

        List<StockMainEntity> stockMainList = stockMainService.queryList(query);
        int total = stockMainService.queryTotal(query);

        PageUtils pageUtil = new PageUtils(stockMainList, total, query.getLimit(), query.getPage());

        return R.ok().put("page", pageUtil);
    }

    /**
     * 查看信息
     */
    @RequestMapping("/info/{keyId}")
    @RequiresPermissions("stockmain:info")
    public R info(@PathVariable("keyId") Long keyId) {
        StockMainEntity stockMain = stockMainService.queryObject(keyId);

        return R.ok().put("stockMain", stockMain);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("stockmain:save")
    public R save(@RequestBody StockMainEntity stockMain) {
        stockMainService.save(stockMain);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("stockmain:update")
    public R update(@RequestBody StockMainEntity stockMain) {
        stockMainService.update(stockMain);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("stockmain:delete")
    public R delete(@RequestBody Long[] keyIds) {
        stockMainService.deleteBatch(keyIds);

        return R.ok();
    }

    /**
     * 查看所有列表
     */
    @RequestMapping("/queryAll")
    public R queryAll(@RequestParam Map<String, Object> params) {

        List<StockMainEntity> list = stockMainService.queryList(params);

        return R.ok().put("list", list);
    }
}
