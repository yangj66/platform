package com.platform.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.platform.entity.ChauffeurEntity;
import com.platform.service.ChauffeurService;
import com.platform.utils.PageUtils;
import com.platform.utils.Query;
import com.platform.utils.R;

/**
 * Controller
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-04-25 10:48:03
 */
@RestController
@RequestMapping("chauffeur")
public class ChauffeurController {
    @Autowired
    private ChauffeurService chauffeurService;

    /**
     * 查看列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("chauffeur:list")
    public R list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);

        List<ChauffeurEntity> chauffeurList = chauffeurService.queryList(query);
        int total = chauffeurService.queryTotal(query);

        PageUtils pageUtil = new PageUtils(chauffeurList, total, query.getLimit(), query.getPage());

        return R.ok().put("page", pageUtil);
    }

    /**
     * 查看信息
     */
    @RequestMapping("/info/{keyId}")
    @RequiresPermissions("chauffeur:info")
    public R info(@PathVariable("keyId") Integer keyId) {
        ChauffeurEntity chauffeur = chauffeurService.queryObject(keyId);

        return R.ok().put("chauffeur", chauffeur);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("chauffeur:save")
    public R save(@RequestBody ChauffeurEntity chauffeur) {
        chauffeurService.save(chauffeur);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("chauffeur:update")
    public R update(@RequestBody ChauffeurEntity chauffeur) {
        chauffeurService.update(chauffeur);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("chauffeur:delete")
    public R delete(@RequestBody Integer[] keyIds) {
        chauffeurService.deleteBatch(keyIds);

        return R.ok();
    }

    /**
     * 查看所有列表
     */
    @RequestMapping("/queryAll")
    public R queryAll(@RequestParam Map<String, Object> params) {

        List<ChauffeurEntity> list = chauffeurService.queryList(params);

        return R.ok().put("list", list);
    }
}
