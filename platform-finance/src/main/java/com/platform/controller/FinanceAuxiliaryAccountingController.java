package com.platform.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.platform.entity.FinanceAuxiliaryAccountingEntity;
import com.platform.service.FinanceAuxiliaryAccountingService;
import com.platform.utils.PageUtils;
import com.platform.utils.Query;
import com.platform.utils.R;

/**
 * Controller
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2018-07-02 16:23:52
 */
@RestController
@RequestMapping("financeauxiliaryaccounting")
public class FinanceAuxiliaryAccountingController {
    @Autowired
    private FinanceAuxiliaryAccountingService financeAuxiliaryAccountingService;

    /**
     * 查看列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("financeauxiliaryaccounting:list")
    public R list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);

        List<FinanceAuxiliaryAccountingEntity> financeAuxiliaryAccountingList = financeAuxiliaryAccountingService.queryList(query);
        int total = financeAuxiliaryAccountingService.queryTotal(query);

        PageUtils pageUtil = new PageUtils(financeAuxiliaryAccountingList, total, query.getLimit(), query.getPage());

        return R.ok().put("page", pageUtil);
    }

    /**
     * 查看信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("financeauxiliaryaccounting:info")
    public R info(@PathVariable("id") Integer id) {
        FinanceAuxiliaryAccountingEntity financeAuxiliaryAccounting = financeAuxiliaryAccountingService.queryObject(id);

        return R.ok().put("financeAuxiliaryAccounting", financeAuxiliaryAccounting);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("financeauxiliaryaccounting:save")
    public R save(@RequestBody FinanceAuxiliaryAccountingEntity financeAuxiliaryAccounting) {
        financeAuxiliaryAccountingService.save(financeAuxiliaryAccounting);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("financeauxiliaryaccounting:update")
    public R update(@RequestBody FinanceAuxiliaryAccountingEntity financeAuxiliaryAccounting) {
        financeAuxiliaryAccountingService.update(financeAuxiliaryAccounting);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("financeauxiliaryaccounting:delete")
    public R delete(@RequestBody Integer[]ids) {
        financeAuxiliaryAccountingService.deleteBatch(ids);

        return R.ok();
    }

    /**
     * 查看所有列表
     */
    @RequestMapping("/queryAll")
    public R queryAll(@RequestParam Map<String, Object> params) {

        List<FinanceAuxiliaryAccountingEntity> list = financeAuxiliaryAccountingService.queryList(params);

        return R.ok().put("list", list);
    }
}
