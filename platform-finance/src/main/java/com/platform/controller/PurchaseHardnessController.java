package com.platform.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.platform.entity.PurchaseHardnessEntity;
import com.platform.service.PurchaseHardnessService;
import com.platform.utils.PageUtils;
import com.platform.utils.Query;
import com.platform.utils.R;

/**
 * Controller
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-02-18 21:26:57
 */
@RestController
@RequestMapping("purchasehardness")
public class PurchaseHardnessController {
    @Autowired
    private PurchaseHardnessService purchaseHardnessService;

    /**
     * 查看列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("purchasehardness:list")
    public R list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);

        List<PurchaseHardnessEntity> purchaseHardnessList = purchaseHardnessService.queryList(query);
        int total = purchaseHardnessService.queryTotal(query);

        PageUtils pageUtil = new PageUtils(purchaseHardnessList, total, query.getLimit(), query.getPage());

        return R.ok().put("page", pageUtil);
    }

    /**
     * 查看信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("purchasehardness:info")
    public R info(@PathVariable("id") Integer id) {
        PurchaseHardnessEntity purchaseHardness = purchaseHardnessService.queryObject(id);

        return R.ok().put("purchaseHardness", purchaseHardness);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("purchasehardness:save")
    public R save(@RequestBody PurchaseHardnessEntity purchaseHardness) {
        purchaseHardnessService.save(purchaseHardness);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("purchasehardness:update")
    public R update(@RequestBody PurchaseHardnessEntity purchaseHardness) {
        purchaseHardnessService.update(purchaseHardness);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("purchasehardness:delete")
    public R delete(@RequestBody Integer[] ids) {
        purchaseHardnessService.deleteBatch(ids);

        return R.ok();
    }

    /**
     * 查看所有列表
     */
    @RequestMapping("/queryAll")
    public R queryAll(@RequestParam Map<String, Object> params) {

        List<PurchaseHardnessEntity> list = purchaseHardnessService.queryList(params);

        return R.ok().put("list", list);
    }
}
