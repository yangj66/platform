package com.platform.controller;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.platform.entity.PurchaseGoodsStockInEntity;
import com.platform.entity.SysUserEntity;
import com.platform.service.PurchaseGoodsStockInService;
import com.platform.utils.*;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.platform.entity.PurchaseGoodsStockOutEntity;
import com.platform.service.PurchaseGoodsStockOutService;

/**
 * Controller
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-02-18 21:26:57
 */
@RestController
@RequestMapping("purchasegoodsstockout")
public class PurchaseGoodsStockOutController {
    @Autowired
    private PurchaseGoodsStockOutService purchaseGoodsStockOutService;

    /**
     * 查看列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("purchasegoodsstockout:list")
    public R list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);

        List<PurchaseGoodsStockOutEntity> purchaseGoodsStockOutList = purchaseGoodsStockOutService.queryList(query);
        int total = purchaseGoodsStockOutService.queryTotal(query);

        PageUtils pageUtil = new PageUtils(purchaseGoodsStockOutList, total, query.getLimit(), query.getPage());

        return R.ok().put("page", pageUtil);
    }

    /**
     * 查看信息
     */
    @RequestMapping("/info/{keyId}")
    @RequiresPermissions("purchasegoodsstockout:info")
    public R info(@PathVariable("keyId") Long keyId) {
        PurchaseGoodsStockOutEntity purchaseGoodsStockOut = purchaseGoodsStockOutService.queryObject(keyId);

        return R.ok().put("purchaseGoodsStockOut", purchaseGoodsStockOut);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("purchasegoodsstockout:save")
    public R save(@RequestBody PurchaseGoodsStockOutEntity purchaseGoodsStockOut) {
        //purchaseGoodsStockOutService.save(purchaseGoodsStockOut);
        Date now = new Date();
        //用户
        SysUserEntity sysUserEntity = ShiroUtils.getUserEntity();
        List inList = purchaseGoodsStockOut.getList();

        //判断stockInId是否重复
        Map params = new HashMap();
        params.put("stockOutId",purchaseGoodsStockOut.getStockOutId());
        List<PurchaseGoodsStockOutEntity> list = purchaseGoodsStockOutService.queryList(params);
        if(list!=null && list.size()>0)
        {
            return R.error("出库编号重复，请重新打开新增页面");
        }

        Map map = new HashMap();
        map.put("stockOutId",purchaseGoodsStockOut.getStockOutId());
        map.put("inputDate",now);
        map.put("stockOutDate",purchaseGoodsStockOut.getStockOutDate());
        map.put("stockOutResponsier",sysUserEntity.getUserId());
        map.put("stockOutMemo",purchaseGoodsStockOut.getStockOutMemo());
        map.put("stockOutOperatorId",sysUserEntity.getUserId());
        map.put("telephone",purchaseGoodsStockOut.getTelephone());
        map.put("contacter",purchaseGoodsStockOut.getContacter());
        map.put("carId",purchaseGoodsStockOut.getCarId());
        map.put("chauffeur",purchaseGoodsStockOut.getChauffeur());
        map.put("address",purchaseGoodsStockOut.getAddress());
        map.put("projectName",purchaseGoodsStockOut.getProjectName());
        map.put("outPerson",purchaseGoodsStockOut.getOutPerson());

        map.put("inList",inList);

        purchaseGoodsStockOutService.saveMap(map);


        //后面写保存方法

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("purchasegoodsstockout:update")
    public R update(@RequestBody PurchaseGoodsStockOutEntity purchaseGoodsStockOut) {
        purchaseGoodsStockOutService.update(purchaseGoodsStockOut);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("purchasegoodsstockout:delete")
    public R delete(@RequestBody Long[] keyIds) {
        purchaseGoodsStockOutService.deleteBatch(keyIds);

        return R.ok();
    }


    /**
     * 删除
     */
    @RequestMapping("/count")
    //@RequiresPermissions("purchasegoodsstockout:delete")
    public R count(@RequestBody Long[] keyIds, @RequestParam Double unitPrice) {
        purchaseGoodsStockOutService.count(keyIds, unitPrice);
        return R.ok();
    }

    /**
     * 查看所有列表
     */
    @RequestMapping("/queryAll")
    public R queryAll(@RequestParam Map<String, Object> params) {

        List<PurchaseGoodsStockOutEntity> list = purchaseGoodsStockOutService.queryList(params);

        return R.ok().put("list", list);
    }

    /**
     * 获取一些基础参数
     */
    @RequestMapping("/getInitData")
    public R createId() {
        /*String id = IdUtil.createIdByDateNew();
        //用户
        //SysUserEntity sysUserEntity = ShiroUtils.getUserEntity();

        Map map = new HashMap();
        map.put("id","SK"+id);
        //map.put("user", sysUserEntity);
        return R.ok().put("map", map);*/
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(new Date());
        dateString = dateString.substring(0,7);
        StringBuilder stockInId = new StringBuilder("SK-");
        stockInId.append(dateString);
        stockInId.append("-");

        int id = 1;
        String tid = purchaseGoodsStockOutService.getMaxId(stockInId.toString());
        if(tid != null)
        {
            id = Integer.parseInt(tid.substring(tid.lastIndexOf("-")+1));
            id++;
        }
        DecimalFormat df = new DecimalFormat("0000");
        stockInId.append(df.format(id));//前缀加上后面的数字

        Map map = new HashMap();
        map.put("id",stockInId.toString());
        return R.ok().put("map", map);
    }


    /**
     * 冲单
     */
    @RequestMapping("/undo")
    @RequiresPermissions("purchasegoodsstockout:delete")
    public R undo(@RequestBody Long[] keyIds) {
        purchaseGoodsStockOutService.undoBatch(keyIds);

        return R.ok();
    }
}
