package com.platform.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.platform.entity.FinanceAuxiliaryAccountingDetailEntity;
import com.platform.service.FinanceAuxiliaryAccountingDetailService;
import com.platform.utils.PageUtils;
import com.platform.utils.Query;
import com.platform.utils.R;

/**
 * Controller
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2018-07-02 10:21:17
 */
@RestController
@RequestMapping("financeauxiliaryaccountingdetail")
public class FinanceAuxiliaryAccountingDetailController {
    @Autowired
    private FinanceAuxiliaryAccountingDetailService financeAuxiliaryAccountingDetailService;

    /**
     * 查看列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("financeauxiliaryaccountingdetail:list")
    public R list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);

        List<FinanceAuxiliaryAccountingDetailEntity> financeAuxiliaryAccountingDetailList = financeAuxiliaryAccountingDetailService.queryList(query);
        int total = financeAuxiliaryAccountingDetailService.queryTotal(query);

        PageUtils pageUtil = new PageUtils(financeAuxiliaryAccountingDetailList, total, query.getLimit(), query.getPage());

        return R.ok().put("page", pageUtil);
    }

    /**
     * 查看信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("financeauxiliaryaccountingdetail:info")
    public R info(@PathVariable("id") Integer id) {
        FinanceAuxiliaryAccountingDetailEntity financeAuxiliaryAccountingDetail = financeAuxiliaryAccountingDetailService.queryObject(id);

        return R.ok().put("financeAuxiliaryAccountingDetail", financeAuxiliaryAccountingDetail);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("financeauxiliaryaccountingdetail:save")
    public R save(@RequestBody FinanceAuxiliaryAccountingDetailEntity financeAuxiliaryAccountingDetail) {
        financeAuxiliaryAccountingDetailService.save(financeAuxiliaryAccountingDetail);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("financeauxiliaryaccountingdetail:update")
    public R update(@RequestBody FinanceAuxiliaryAccountingDetailEntity financeAuxiliaryAccountingDetail) {
        financeAuxiliaryAccountingDetailService.update(financeAuxiliaryAccountingDetail);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("financeauxiliaryaccountingdetail:delete")
    public R delete(@RequestBody Integer[]ids) {
        financeAuxiliaryAccountingDetailService.deleteBatch(ids);

        return R.ok();
    }

    /**
     * 查看所有列表
     */
    @RequestMapping("/queryAll")
    public R queryAll(@RequestParam Map<String, Object> params) {

        List<FinanceAuxiliaryAccountingDetailEntity> list = financeAuxiliaryAccountingDetailService.queryList(params);

        return R.ok().put("list", list);
    }

    /**
     * 启用
     */
    @RequestMapping("/enUse")
    public R enSale(@RequestBody Integer id) {
        financeAuxiliaryAccountingDetailService.enUse(id);

        return R.ok();
    }

    /**
     * 禁用
     */
    @RequestMapping("/unUse")
    public R unSale(@RequestBody Integer id) {
        financeAuxiliaryAccountingDetailService.unUse(id);

        return R.ok();
    }
}
