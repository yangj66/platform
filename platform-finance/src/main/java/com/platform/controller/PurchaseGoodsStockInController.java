package com.platform.controller;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.platform.entity.SysUserEntity;
import com.platform.utils.*;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.platform.entity.PurchaseGoodsStockInEntity;
import com.platform.service.PurchaseGoodsStockInService;

/**
 * Controller
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-02-12 13:25:53
 */
@RestController
@RequestMapping("purchasegoodsstockin")
public class PurchaseGoodsStockInController {
    @Autowired
    private PurchaseGoodsStockInService purchaseGoodsStockInService;

    /**
     * 查看列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("purchasegoodsstockin:list")
    public R list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);

        List<PurchaseGoodsStockInEntity> purchaseGoodsStockInList = purchaseGoodsStockInService.queryList(query);
        int total = purchaseGoodsStockInService.queryTotal(query);

        PageUtils pageUtil = new PageUtils(purchaseGoodsStockInList, total, query.getLimit(), query.getPage());

        return R.ok().put("page", pageUtil);
    }

    /**
     * 查看信息
     */
    @RequestMapping("/info/{keyId}")
    @RequiresPermissions("purchasegoodsstockin:info")
    public R info(@PathVariable("keyId") Long keyId) {
        PurchaseGoodsStockInEntity purchaseGoodsStockIn = purchaseGoodsStockInService.queryObject(keyId);

        Map params = new HashMap();
        params.put("stockInId",purchaseGoodsStockIn.getStockInId());
        List<PurchaseGoodsStockInEntity> list = purchaseGoodsStockInService.queryList(params);
        purchaseGoodsStockIn.setList(list);

        for(int i=0; i<list.size(); i++) {
            PurchaseGoodsStockInEntity stockIn = list.get(i);
            if(!"0".equals(stockIn.getStatus()) || stockIn.getOutQuantity().doubleValue()>0.0)
            {
                return R.error("出库单："+stockIn.getStockInId()+"已出库，请先冲出库单，再进行修改");
            }
        }

        return R.ok().put("purchaseGoodsStockIn", purchaseGoodsStockIn);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("purchasegoodsstockin:save")
    //PurchaseGoodsStockInEntity purchaseGoodsStockIn
    public R save(@RequestBody PurchaseGoodsStockInEntity purchaseGoodsStockIn) {
        Date now = new Date();
        //用户
        SysUserEntity sysUserEntity = ShiroUtils.getUserEntity();
        List itemList = purchaseGoodsStockIn.getList();

        //判断stockInId是否重复
        Map params = new HashMap();
        params.put("stockInId",purchaseGoodsStockIn.getStockInId());
        List<PurchaseGoodsStockInEntity> list = purchaseGoodsStockInService.queryList(params);
        if(list!=null && list.size()>0)
        {
            return R.error("入库编号重复，请重新打开新增页面");
        }

        Map map = new HashMap();
        map.put("stockInId",purchaseGoodsStockIn.getStockInId());
        map.put("stockInDate",purchaseGoodsStockIn.getStockInDate());
        map.put("stockId",purchaseGoodsStockIn.getStockId());
        map.put("stockInMemo",purchaseGoodsStockIn.getStockInMemo());

        map.put("inputDate",now);
        map.put("stockInResponsier",sysUserEntity.getUserId().toString());

        //map.put("itemNumber",1);
        map.put("supplierId","");
        map.put("itemList",itemList);

        purchaseGoodsStockInService.saveMap(map);
        /*//用户
        SysUserEntity sysUserEntity = ShiroUtils.getUserEntity();
        purchaseGoodsStockIn.setStockInResponsier(sysUserEntity.getUserId().toString());
        purchaseGoodsStockIn.setInputDate(new Date());
        purchaseGoodsStockInService.save(purchaseGoodsStockIn);*/

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("purchasegoodsstockin:update")
    public R update(@RequestBody PurchaseGoodsStockInEntity purchaseGoodsStockIn) {
//        purchaseGoodsStockInService.update(purchaseGoodsStockIn);

//判断有出库则不能进行修改
//        Map params = new HashMap();
//        params.put("stockInId",purchaseGoodsStockIn.getStockInId());
//        List<PurchaseGoodsStockInEntity> list = purchaseGoodsStockInService.queryList(params);
//        purchaseGoodsStockIn.setList(list);

        Date now = new Date();
        //用户
        SysUserEntity sysUserEntity = ShiroUtils.getUserEntity();
        List itemList = purchaseGoodsStockIn.getList();

        Map map = new HashMap();
        map.put("stockInId",purchaseGoodsStockIn.getStockInId());
        map.put("stockInDate",purchaseGoodsStockIn.getStockInDate());
        map.put("stockId",purchaseGoodsStockIn.getStockId());
        map.put("stockInMemo",purchaseGoodsStockIn.getStockInMemo());

        map.put("inputDate",now);
        map.put("stockInResponsier",sysUserEntity.getUserId().toString());

        //map.put("itemNumber",1);
        map.put("supplierId","");
        map.put("itemList",itemList);

        //先删除
        purchaseGoodsStockInService.deleteByStockInId(purchaseGoodsStockIn.getStockInId());
        //再新增
        purchaseGoodsStockInService.saveMap(map);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("purchasegoodsstockin:delete")
    public R delete(@RequestBody Long[] keyIds) {
        purchaseGoodsStockInService.deleteBatch(keyIds);

        return R.ok();
    }

    /**
     * 查看所有列表
     */
    @RequestMapping("/queryAll")
    public R queryAll(@RequestParam Map<String, Object> params) {

        List<PurchaseGoodsStockInEntity> list = purchaseGoodsStockInService.queryList(params);

        return R.ok().put("list", list);
    }


    /**
     * 获取一些基础参数
     */
    @RequestMapping("/getInitData")
    public R createId() {
        //String id = IdUtil.createIdByDateNew();
        //用户
        //SysUserEntity sysUserEntity = ShiroUtils.getUserEntity();

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(new Date());
        dateString = dateString.replace("-","");
        dateString = dateString.substring(2,6);
        StringBuilder stockInId = new StringBuilder("RK-");
        stockInId.append(dateString);
        stockInId.append("-");

        int id = 1;
        String tid = purchaseGoodsStockInService.getMaxId(stockInId.toString());
        if(tid != null)
        {
            id = Integer.parseInt(tid.substring(tid.lastIndexOf("-")+1));
            id++;
        }
        DecimalFormat df = new DecimalFormat("0000");
        stockInId.append(df.format(id));//前缀加上后面的数字

        Map map = new HashMap();
        map.put("id",stockInId.toString());
        //map.put("id","RK"+id);
        //map.put("user", sysUserEntity);
        return R.ok().put("map", map);
    }
}
