/*
 * 类名称:OrDeleteLogController.java
 * 包名称:com.platform.controller
 *
 * 修改履历:
 *     日期                       修正者        主要内容
 *     2019-10-09 09:10:55        lipengjun     初版做成
 *
 * Copyright (c) 2019-2019 微同软件
 */
package com.platform.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.platform.annotation.SysLog;
import com.platform.utils.R;
import com.platform.controller.AbstractController;
import com.platform.entity.OrDeleteLogEntity;
import com.platform.service.OrDeleteLogService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * Controller
 *
 * @author lipengjun
 * @date 2019-10-09 09:10:55
 */
@RestController
@RequestMapping("stock/ordeletelog")
public class OrDeleteLogController extends AbstractController {
    @Autowired
    private OrDeleteLogService orDeleteLogService;

    /**
     * 查看所有列表
     *
     * @param params 查询参数
     * @return R
     */
    @RequestMapping("/queryAll")
    @RequiresPermissions("stock:ordeletelog:list")
    public R queryAll(@RequestParam Map<String, Object> params) {
        List<OrDeleteLogEntity> list = orDeleteLogService.queryAll(params);

        return R.ok().put("list", list);
    }

    /**
     * 分页查询
     *
     * @param params 查询参数
     * @return R
     */
    @GetMapping("/list")
    @RequiresPermissions("stock:ordeletelog:list")
    public R list(@RequestParam Map<String, Object> params) {
        Page page = orDeleteLogService.queryPage(params);

        return R.ok().put("page", page);
    }

    /**
     * 根据主键查询详情
     *
     * @param id 主键
     * @return R
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("stock:ordeletelog:info")
    public R info(@PathVariable("id") Long id) {
        OrDeleteLogEntity orDeleteLog = orDeleteLogService.getById(id);

        return R.ok().put("ordeletelog", orDeleteLog);
    }

    /**
     * 新增
     *
     * @param orDeleteLog orDeleteLog
     * @return R
     */
    @SysLog("新增")
    @RequestMapping("/save")
    @RequiresPermissions("stock:ordeletelog:save")
    public R save(@RequestBody OrDeleteLogEntity orDeleteLog) {

        orDeleteLogService.add(orDeleteLog);

        return R.ok();
    }

    /**
     * 修改
     *
     * @param orDeleteLog orDeleteLog
     * @return R
     */
    @SysLog("修改")
    @RequestMapping("/update")
    @RequiresPermissions("stock:ordeletelog:update")
    public R update(@RequestBody OrDeleteLogEntity orDeleteLog) {

        orDeleteLogService.update(orDeleteLog);

        return R.ok();
    }

    /**
     * 根据主键删除
     *
     * @param ids ids
     * @return R
     */
    @SysLog("删除")
    @RequestMapping("/delete")
    @RequiresPermissions("stock:ordeletelog:delete")
    public R delete(@RequestBody Long[] ids) {
        orDeleteLogService.deleteBatch(ids);

        return R.ok();
    }
}
