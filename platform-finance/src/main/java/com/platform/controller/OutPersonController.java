package com.platform.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.platform.entity.OutPersonEntity;
import com.platform.service.OutPersonService;
import com.platform.utils.PageUtils;
import com.platform.utils.Query;
import com.platform.utils.R;

/**
 * Controller
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2019-04-25 10:48:03
 */
@RestController
@RequestMapping("outperson")
public class OutPersonController {
    @Autowired
    private OutPersonService outPersonService;

    /**
     * 查看列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("outperson:list")
    public R list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);

        List<OutPersonEntity> outPersonList = outPersonService.queryList(query);
        int total = outPersonService.queryTotal(query);

        PageUtils pageUtil = new PageUtils(outPersonList, total, query.getLimit(), query.getPage());

        return R.ok().put("page", pageUtil);
    }

    /**
     * 查看信息
     */
    @RequestMapping("/info/{keyId}")
    @RequiresPermissions("outperson:info")
    public R info(@PathVariable("keyId") Integer keyId) {
        OutPersonEntity outPerson = outPersonService.queryObject(keyId);

        return R.ok().put("outPerson", outPerson);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("outperson:save")
    public R save(@RequestBody OutPersonEntity outPerson) {
        outPersonService.save(outPerson);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("outperson:update")
    public R update(@RequestBody OutPersonEntity outPerson) {
        outPersonService.update(outPerson);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("outperson:delete")
    public R delete(@RequestBody Integer[] keyIds) {
        outPersonService.deleteBatch(keyIds);

        return R.ok();
    }

    /**
     * 查看所有列表
     */
    @RequestMapping("/queryAll")
    public R queryAll(@RequestParam Map<String, Object> params) {

        List<OutPersonEntity> list = outPersonService.queryList(params);

        return R.ok().put("list", list);
    }
}
