var tableIns;
var table;
layui.use('table', function(){
    table = layui.table;
    tableIns = table.render({
        elem: '#test'
		,url: '../purchasegoodsmain/list'
		,where: {sidx: null, order: 'asc'}
        //,toolbar: '#toolbarDemo'
		,parseData: function(res){ //res 即为原始返回的数据
			return {
				"code": res.code, //解析接口状态
				"msg": '', //解析提示文本
				"count": res.page.totalCount, //解析数据长度
				"data": res.page.list //解析数据列表
			};
    	}
        ,cols: [[
            {type:'radio'}
            ,{field:'productId', width:100, title: '物品ID', sort: true}
            ,{field:'productMeasurement', width:80, title: '单位'}
            ,{field:'productName', width:150, title: '物品名'}

            ,{field:'productDescription', width:150, title: '物品描述'}
            ,{field:'productMemo', width:150, title: '备注'}
            ,{field:'productClass', width:100, title: '强度'}
            ,{field:'guige', width:150, title: '规格'}
            ,{field:'daima', width:80, title: '代码'}
        ]]
        ,page: true
    });

    //头工具栏事件
    /*table.on('toolbar(test)', function(obj){
        var checkStatus = table.checkStatus(obj.config.id); //获取选中行状态
        switch(obj.event){
            case 'getCheckData':
                var data = checkStatus.data;  //获取选中行数据
                layer.alert(JSON.stringify(data));
                break;
        };
    });*/

});


function getProductInfo(){
    var checkStatus = table.checkStatus('test')
        ,data = checkStatus.data;
	return data;
}


let vm = new Vue({
    el: '#rrapp',
    data: {
        q: {
            name: ''
        }
    },
    methods: {
        query: function () {
            //vm.reload();
            tableIns.reload({
                where: { //设定异步数据接口的额外参数，任意设
                    name: vm.q.name,
                    sidx: null,
					order: 'asc'
                }
                ,page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        },
        reloadSearch: function() {
            vm.q = {
                name: ''
            }
        }
    }
});