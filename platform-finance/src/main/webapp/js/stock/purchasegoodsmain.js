$(function () {
    $("#jqGrid").Grid({
        url: '../purchasegoodsmain/list',
        colModel: [
			{label: 'keyId', name: 'keyId', index: 'key_id', key: true, hidden: true},
			{label: '代码', name: 'productId', index: 'product_id', width: 80},
			{label: '度量单位', name: 'productMeasurement', index: 'product_measurement', width: 80},
			{label: '物品名称', name: 'productName', index: 'product_name', width: 80},
			{label: '物品详细描述', name: 'productDescription', index: 'product_description', width: 80},
			{label: '物品备注', name: 'productMemo', index: 'product_memo', width: 80},
			{label: '强度', name: 'productClass', index: 'product_class', width: 80},
            {label: '规格', name: 'guige', index: 'guige', width: 80},
            {label: '代码', name: 'daima', index: 'daima', width: 80},
			{label: '状态', name: 'status', index: 'status', width: 80}]
    });
});

let vm = new Vue({
	el: '#rrapp',
	data: {
        showList: true,
        title: null,
		purchaseGoodsMain: {},
		ruleValidate: {
			name: [
				{required: true, message: '名称不能为空', trigger: 'blur'}
			]
		},
		q: {
		    name: ''
		}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function () {
			vm.showList = false;
			vm.title = "新增";
			vm.purchaseGoodsMain = {};
		},
		update: function (event) {
            let keyId = getSelectedRow("#jqGrid");
			if (keyId == null) {
				return;
			}
			vm.showList = false;
            vm.title = "修改";

            vm.getInfo(keyId)
		},
		saveOrUpdate: function (event) {
            let url = vm.purchaseGoodsMain.keyId == null ? "../purchasegoodsmain/save" : "../purchasegoodsmain/update";
            Ajax.request({
			    url: url,
                params: JSON.stringify(vm.purchaseGoodsMain),
                type: "POST",
			    contentType: "application/json",
                successCallback: function (r) {
                    alert('操作成功', function (index) {
                        vm.reload();
                    });
                }
			});
		},
		del: function (event) {
            let keyIds = getSelectedRows("#jqGrid");
			if (keyIds == null){
				return;
			}

			confirm('确定要删除选中的记录？', function () {
                Ajax.request({
				    url: "../purchasegoodsmain/delete",
                    params: JSON.stringify(keyIds),
                    type: "POST",
				    contentType: "application/json",
                    successCallback: function () {
                        alert('操作成功', function (index) {
                            vm.reload();
                        });
					}
				});
			});
		},
		getInfo: function(keyId){
            Ajax.request({
                url: "../purchasegoodsmain/info/"+keyId,
                async: true,
                successCallback: function (r) {
                    vm.purchaseGoodsMain = r.purchaseGoodsMain;
                }
            });
		},
		reload: function (event) {
			vm.showList = true;
            let page = $("#jqGrid").jqGrid('getGridParam', 'page');
			$("#jqGrid").jqGrid('setGridParam', {
                postData: {'name': vm.q.name},
                page: page
            }).trigger("reloadGrid");
            vm.handleReset('formValidate');
		},
        reloadSearch: function() {
            vm.q = {
                name: ''
            }
            vm.reload();
        },
        handleSubmit: function (name) {
            handleSubmitValidate(this, name, function () {
                vm.saveOrUpdate()
            });
        },
        handleReset: function (name) {
            handleResetForm(this, name);
        }
	}
});