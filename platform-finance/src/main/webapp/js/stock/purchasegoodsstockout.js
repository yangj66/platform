$(function () {
    $("#jqGrid").Grid({
        url: '../purchasegoodsstockout/list',
        colModel: [
			{label: 'keyId', name: 'keyId', index: 'key_id', key: true, hidden: true},
            {label: '出库编号', name: 'stockOutId', index: 'stock_out_id', width: 160},
            {label: '出库日期', name: 'stockOutDate', index: 'stock_out_date', width: 80, formatter: function (value) {
                return transDate(value,'yyyy-MM-dd');
            }},
            {label: '入库批号', name: 'batchNumber', index: 'batch_number', width: 100},
            {label: '物品', name: 'pProductName', index: 'p_product_name', width: 120},
            {label: '强度', name: 'pProductClass', index: 'p_product_class', width: 40},
            {label: '规格', name: 'pGuige', index: 'p_guige', width: 40},
            {label: '出库数量', name: 'stockOutQuantity', index: 'stock_out_quantity', width: 100},
            {label: '净重', name: 'netWeight', index: 'net_Weight', width: 50},
            {label: '皮重', name: 'tareWeight', index: 'tare_Weight', width: 50},
            {label: '毛重', name: 'grossWeight', index: 'gross_Weight', width: 50},
            {label: '单价', name: 'unitPrice', index: 'unit_price', width: 50, cellattr: setBule},
            {label: '金额', name: 'price', index: 'price', width: 50, cellattr: setBule},
            {label: '联系电话', name: 'telephone', index: 'telephone', width: 80},
            {label: '联系人', name: 'contacter', index: 'contacter', width: 60},
            {label: '承运', name: 'carId', index: 'carId', width: 80},
            {label: '司机', name: 'chauffeur', index: 'chauffeur', width: 60},
            {label: '送货地址', name: 'address', index: 'address', width: 80},
			{label: '制单人', name: 'responsier', index: 'responsier', width: 60},
            {label: '出库人', name: 'outPerson', index: 'out_person', width: 60},
			{label: '仓库', name: 'stockDescription', index: 'stock_id', width: 80},
			{label: '出库备注', name: 'stockOutMemo', index: 'stock_out_memo', width: 80},
			{label: '出库操作人', name: 'operator', index: 'operator', width: 60},
            {label: '项目名称', name: 'projectName', index: 'project_name', width: 60},
            {label: '条目备注', name: 'itemMemo', index: 'item_memo', width: 60},

            {label: '入库编号', name: 'stockInId', index: 'stock_in_id', width: 160},
            {label: '入库条目号', name: 'stockInItemNumber', index: 'stock_in_item_number', width: 40}
        ],
        gridComplete:function(){
            if($("#isCountPermission").val()=='false')
            {
                $("#jqGrid").hideCol("unitPrice");
                $("#jqGrid").hideCol("price");
            }else{
                $("#jqGrid").showCol("unitPrice");
                $("#jqGrid").showCol("price");
            }
        }
    })

    //$("#jqGrid").setGridParam().hideCol("unitPrice");
    // $("#jqGrid").setGridParam().showCol("unitPrice");
});

function setBule(rowId, val, rawObject, cm, rdata) {
    return "style='color:blue'";
}

$(window).on('resize', function (e) {

    var $that = $(this);
    //console.log($that.height());
    $('#rrapp').height($that.height());
}).resize();


let vm = new Vue({
	el: '#rrapp',
	data: {
        showList: true,
        title: null,
		purchaseGoodsStockOut: {},
		ruleValidate: {
			name: [
				{required: true, message: '名称不能为空', trigger: 'blur'}
			]
		},
		q: {
		    name: ''
            ,outDate:''
            ,projectName:''
		},
        inList: [],
        qIn: {
            //status: ['0','1']
            stockQty:'yes'
            ,nameadd: ''//新增出库界面的查询条件
        },
        checkAll: false,
        suppliers:[],
        products: [],//产品
        projects:[],//项目信息
        carIds: [],//承运
        chauffeurs: [],//司机
        outPersons: [],//发货人
	},

    /*computed: {
         // 计算属性的 getter
         getStockOutQuantity: function () {
         // `this` 指向 vm 实例
         for(var i=0; i<this.inList.length;i++)
         {
             this.$forceUpdate();
            var item = this.inList[i];
            Vue.set(item,'stockOutQuantity',item.grossWeight - item.tareWeight)
         }
         }
    },*/
    mounted: function () {
        //this.getSuppliers();//得到供应商数据
        //this.getProducts();//得到产品数据
        this.getProjects();//得到项目信息
        this.getCarIds();
        this.getchauffeurs();
        this.getOutPersons();
    },
	methods: {
        //根据净重和皮重 算出毛重  净重+皮重 = 毛重
        getGrossWeight: function (item) {
            // `this` 指向 vm 实例
            this.$forceUpdate();
            Vue.set(item,'grossWeight',item.netWeight + item.tareWeight)
            //this.$set('item.stockOutQuantity', item.grossWeight - item.tareWeight);
        },

		query: function () {
			vm.reload();
		},
		add: function () {
			vm.showList = false;
			vm.title = "新增";
			vm.purchaseGoodsStockOut = {};
			vm.getInList();
            vm.getInitData();
		},
		update: function (event) {
            let keyId = getSelectedRow("#jqGrid");
			if (keyId == null) {
				return;
			}
			vm.showList = false;
            vm.title = "修改";
            vm.inList=[];
            vm.getInfo(keyId)
		},
        count: function (event) {
            let keyIds = getSelectedRows("#jqGrid");
            if (keyIds == null){
                return;
            }

            layer.prompt(
                {title: '请填入单价'
                 ,btn: ['计算', '取消']
                }
                , function(value, index){

                    Ajax.request({
                        url: "../purchasegoodsstockout/count?unitPrice="+value,
                        params: JSON.stringify(keyIds),
                        type: "POST",
                        contentType: "application/json",
                        successCallback: function () {
                            alert('操作成功', function (index1) {
                                layer.close(index);
                                vm.reload();
                            });
                        }
                    });
            });

        },
		saveOrUpdate: function (event) {
            let url = vm.purchaseGoodsStockOut.keyId == null ? "../purchasegoodsstockout/save" : "../purchasegoodsstockout/update";
            vm.purchaseGoodsStockOut.list = vm.inList;
            //console.log(vm.purchaseGoodsStockOut);
            Ajax.request({
			    url: url,
                params: JSON.stringify(vm.purchaseGoodsStockOut),
                type: "POST",
			    contentType: "application/json",
                successCallback: function (r) {
                    alert('操作成功', function (index) {
                        vm.reload();
                    });
                }
			});
		},
		del: function (event) {
            let keyIds = getSelectedRows("#jqGrid");
			if (keyIds == null){
				return;
			}

			confirm('确定要删除选中的记录？', function () {
                Ajax.request({
				    url: "../purchasegoodsstockout/delete",
                    params: JSON.stringify(keyIds),
                    type: "POST",
				    contentType: "application/json",
                    successCallback: function () {
                        alert('操作成功', function (index) {
                            vm.reload();
                        });
					}
				});
			});
		},
        print: function (event) {
            let keyId = getSelectedRow("#jqGrid");
            if (keyId == null) {
                return;
            }

            var id = jQuery("#jqGrid").jqGrid('getGridParam', 'selrow');
            var ret = jQuery("#jqGrid").jqGrid('getRowData', id);

            var newWeb=window.open('_blank');
            var url = "http://192.168.2.254:8082/glassReport/reportJsp/showReport.jsp?rpx=new_purchase_goods_stock_out.rpx&stockoutid="+ret.stockOutId;
            newWeb.location=url;

        },
		getInfo: function(keyId){
            Ajax.request({
                url: "../purchasegoodsstockout/info/"+keyId,
                async: true,
                successCallback: function (r) {
                    vm.purchaseGoodsStockOut = r.purchaseGoodsStockOut;
                }
            });
		},
		reload: function (event) {
			vm.showList = true;
            let page = $("#jqGrid").jqGrid('getGridParam', 'page');
            var startDate = '';
            var endDate = '';
            //console.log(vm.q.outDate);
            var sd = vm.q.outDate[0];
            var ed = vm.q.outDate[1];
            if(sd && ed)
            {
                startDate = sd.getFullYear() + '-' + (sd.getMonth() + 1) + '-' + sd.getDate()
                endDate = ed.getFullYear() + '-' + (ed.getMonth() + 1) + '-' + ed.getDate()
            }
            $("#jqGrid").jqGrid('setGridParam', {
                postData: {'name': vm.q.name
                    ,'startDate': startDate
                    ,'endDate': endDate
                    , 'projectName':vm.q.projectName},
                page: page
            }).trigger("reloadGrid");
            vm.handleReset('formValidate');
		},
        reloadSearch: function() {
            vm.q = {
                name: ''
                ,outDate:''
                ,projectName:''
            }
            vm.reload();
        },
        handleSubmit: function (name) {
            console.log("===handleSubmit===")
            //handleSubmitValidate(this, name, function () {
            //    console.log("===saveOrUpdate()===")
                vm.saveOrUpdate();
            //});
        },
        handleReset: function (name) {
            handleResetForm(this, name);
        },

        getInList: function(keyId){
            Ajax.request({
                url: "../purchasegoodsstockin/queryAll",
                //params: JSON.stringify(vm.q.nameadd),
                params: vm.qIn,
                async: true,
                successCallback: function (r) {
                    if(r.code == 0)
                    {
                        vm.inList = r.list;
                        for(var i=0; i<vm.inList.length; i++)
                        {
                            vm.inList[i].checked = false;//初始让所有复选框不选中
                            //vm.inList[i].stockOutQuantity = vm.inList[i].stockQuantity;//初始让所有复选框不选中
                            vm.inList[i].stockOutQuantity = 0;
                            vm.inList[i].grossWeight = 0;
                            vm.inList[i].tareWeight = 0;
                            vm.inList[i].netWeight = 0;
                            vm.inList[i].itemMemo = '';
                        }
                    }
                }
            });
        },
        handleCheckAll:function() {
            this.checkAll = !this.checkAll;
            for(var i=0; i<this.inList.length; i++)
            {
                if (this.checkAll) {
                    this.inList[i].checked = true;
                } else {
                    this.inList[i].checked = false;
                }
            }
		},

        /**
         * 初始化数据
         */
        getInitData: function () {
            Ajax.request({
                url: "../purchasegoodsstockout/getInitData",
                async: true,
                successCallback: function (r) {
                    //console.log(r.id);
                    vm.purchaseGoodsStockOut = {stockOutId:r.map.id,stockOutDate:new Date(),list:[]};
                }
            });
        },

        /**
         * 获取供应商信息
         */
        getSuppliers: function () {
            Ajax.request({
                url: "../suppliermain/queryAll",
                async: true,
                successCallback: function (r) {
                    vm.suppliers = r.list;
                }
            });
        },
        /**
         * 获取产品信息
         */
        getProducts: function () {
            Ajax.request({
                url: "../purchasegoodsmain/queryAll",
                async: true,
                successCallback: function (r) {
                    vm.products = r.list;
                }
            });
        },

        /**
         * 获取项目信息
         */
        getProjects: function () {
            Ajax.request({
                url: "../projectmain/queryAll",
                async: true,
                successCallback: function (r) {
                    vm.projects = r.list;
                }
            });
        },

        /**
         * 获取承运信息
         */
        getCarIds: function () {
            Ajax.request({
                url: "../carid/queryAll",
                async: true,
                successCallback: function (r) {
                    vm.carIds = r.list;
                }
            });
        },

        /**
         * 获取司机信息
         */
        getchauffeurs: function () {
            Ajax.request({
                url: "../chauffeur/queryAll",
                async: true,
                successCallback: function (r) {
                    vm.chauffeurs = r.list;
                }
            });
        },

        /**
         * 获取发货人信息
         */
        getOutPersons: function () {
            Ajax.request({
                url: "../outperson/queryAll",
                async: true,
                successCallback: function (r) {
                    vm.outPersons = r.list;
                }
            });
        },

        /**
         * 改变项目触发信息
         */
        changeProject: function () {
            for(var i=0; i<vm.projects.length; i++)
            {
                if (vm.purchaseGoodsStockOut.projectName == vm.projects[i].projectName) {
                    vm.purchaseGoodsStockOut.telephone = vm.projects[i].tel;
                    vm.purchaseGoodsStockOut.contacter = vm.projects[i].contact;
                    vm.purchaseGoodsStockOut.address = vm.projects[i].address;
                }
            }
        },

        /**
         * 冲单
         * @param event
         */
        undo: function (event) {
            let keyIds = getSelectedRows("#jqGrid");
            if (keyIds == null){
                return;
            }

            confirm('确定要删除选中的记录？', function () {
                Ajax.request({
                    url: "../purchasegoodsstockout/undo",
                    params: JSON.stringify(keyIds),
                    type: "POST",
                    contentType: "application/json",
                    successCallback: function () {
                        alert('操作成功', function (index) {
                            vm.reload();
                        });
                    }
                });
            });
        },

        refreshAdd: function () {
            vm.getInList();
        },

        reloadSearchAdd: function() {
            vm.qIn = {
                stockQty:'yes'
                ,nameadd: ''//新增出库界面的查询条件
                }
            vm.getInList();
        },

	}
});