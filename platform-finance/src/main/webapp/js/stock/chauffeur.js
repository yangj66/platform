$(function () {
    $("#jqGrid").Grid({
        url: '../chauffeur/list',
        colModel: [
			{label: 'keyId', name: 'keyId', index: 'key_id', key: true, hidden: true},
			{label: '司机', name: 'chauffeur', index: 'chauffeur', width: 80}]
    });
});

let vm = new Vue({
	el: '#rrapp',
	data: {
        showList: true,
        title: null,
		chauffeur: {},
		ruleValidate: {
			name: [
				{required: true, message: '名称不能为空', trigger: 'blur'}
			]
		},
		q: {
		    name: ''
		}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function () {
			vm.showList = false;
			vm.title = "新增";
			vm.chauffeur = {};
		},
		update: function (event) {
            let keyId = getSelectedRow("#jqGrid");
			if (keyId == null) {
				return;
			}
			vm.showList = false;
            vm.title = "修改";

            vm.getInfo(keyId)
		},
		saveOrUpdate: function (event) {
            let url = vm.chauffeur.keyId == null ? "../chauffeur/save" : "../chauffeur/update";
            Ajax.request({
			    url: url,
                params: JSON.stringify(vm.chauffeur),
                type: "POST",
			    contentType: "application/json",
                successCallback: function (r) {
                    alert('操作成功', function (index) {
                        vm.reload();
                    });
                }
			});
		},
		del: function (event) {
            let keyIds = getSelectedRows("#jqGrid");
			if (keyIds == null){
				return;
			}

			confirm('确定要删除选中的记录？', function () {
                Ajax.request({
				    url: "../chauffeur/delete",
                    params: JSON.stringify(keyIds),
                    type: "POST",
				    contentType: "application/json",
                    successCallback: function () {
                        alert('操作成功', function (index) {
                            vm.reload();
                        });
					}
				});
			});
		},
		getInfo: function(keyId){
            Ajax.request({
                url: "../chauffeur/info/"+keyId,
                async: true,
                successCallback: function (r) {
                    vm.chauffeur = r.chauffeur;
                }
            });
		},
		reload: function (event) {
			vm.showList = true;
            let page = $("#jqGrid").jqGrid('getGridParam', 'page');
			$("#jqGrid").jqGrid('setGridParam', {
                postData: {'name': vm.q.name},
                page: page
            }).trigger("reloadGrid");
            vm.handleReset('formValidate');
		},
        reloadSearch: function() {
            vm.q = {
                name: ''
            }
            vm.reload();
        },
        handleSubmit: function (name) {
            handleSubmitValidate(this, name, function () {
                vm.saveOrUpdate()
            });
        },
        handleReset: function (name) {
            handleResetForm(this, name);
        }
	}
});