var selectIndex = -1;

$(function () {
    $("#jqGrid").Grid({
        url: '../purchasegoodsstockin/list',
        colModel: [
			{label: 'keyId', name: 'keyId', index: 'key_id', key: true, hidden: true},
			{label: '入库编号', name: 'stockInId', index: 'stock_in_id', width: 110},
            {label: '条目号', name: 'itemNumber', index: 'item_number', width: 40},
            {label: '物品', name: 'pProductName', index: 'p_product_name', width: 100},
            {label: '规格', name: 'pGuige', index: 'p_guige', width: 60},
            {label: '强度', name: 'pProductClass', index: 'p_product_class', width: 60},
            {label: '入库数量', name: 'inQuantity', index: 'in_quantity', width: 50},
            {label: '库存数量', name: 'stockQuantity', index: 'stock_quantity', width: 50},
            {label: '出库数量', name: 'outQuantity', index: 'out_quantity', width: 50},
            {label: '状态', name: 'status', index: 'status', width: 40, formatter: function (value){
                if(value=='0')
                    return '未出库';
                else if(value=='1')
                    return '出库中';
                else if(value=='2')
                    return '已出完';
            }},
            {label: '批号', name: 'batchNumber', index: 'batch_number', width: 120},
            {label: '入库时间', name: 'stockInDate', index: 'stock_in_date', width: 60, formatter: function (value) {
                return transDate(value,'yyyy-MM-dd');
            }},
			{label: '入库人', name: 'username', index: 'username', width: 60},
			{label: '仓库', name: 'stockDescription', index: 'stock_description', width: 80},
			//{label: '供应商', name: 'supplierId', index: 'supplier_id', width: 80},
			{label: '备注', name: 'stockInMemo', index: 'stock_in_memo', width: 80},
            {label: '条目备注', name: 'itemMemo', index: 'item_memo', width: 40}]
    });

});


$(window).on('resize', function (e) {

    var $that = $(this);
    //console.log($that.height());
    $('#rrapp').height($that.height());
}).resize();


let vm = new Vue({
	el: '#rrapp',
	data: {
        showList: true,
        title: null,
		purchaseGoodsStockIn: {list:[]},
		ruleValidate: {
            stockInId: [
				{required: true, message: 'ID不能为空', trigger: 'blur'}
			]
            // ,stockInDate:[
            //     {required: true, message: '入库时间不能为空', trigger: 'blur'}
            // ]
		},
		q: {
		    name: ''
		},

        //lists:[],
        nextId: 0,
        products: [],//产品
        stocks: [],//仓库
       // hardnesses:[],//强度
	},
    mounted: function () {
        //this.getProducts();//得到产品数据
        this.getStocks();//得到仓库数据
        //this.getHardness();//得到强度数据
    },
	methods: {
		query: function () {
			vm.reload();
		},
		add: function () {
			vm.showList = false;
			vm.title = "新增";
			vm.getInitData();
		},
		update: function (event) {
            let keyId = getSelectedRow("#jqGrid");
			if (keyId == null) {
				return;
			}
            // vm.showList = false;
            // vm.title = "修改";

            vm.getInfo(keyId)
		},
		saveOrUpdate: function (event) {
            let url = vm.purchaseGoodsStockIn.keyId == null ? "../purchasegoodsstockin/save" : "../purchasegoodsstockin/update";
            //let obj = [vm.purchaseGoodsStockIn,vm.lists];
            console.log(JSON.stringify(vm.purchaseGoodsStockIn));
            Ajax.request({
			    url: url,
                params: JSON.stringify(vm.purchaseGoodsStockIn),
                type: "POST",
			    contentType: "application/json",
                successCallback: function (r) {
                    alert('操作成功', function (index) {
                        vm.reload();
                    });
                }
			});
		},
		del: function (event) {
            let keyIds = getSelectedRows("#jqGrid");
			if (keyIds == null){
				return;
			}

			confirm('确定要删除选中的记录？', function () {
                Ajax.request({
				    url: "../purchasegoodsstockin/delete",
                    params: JSON.stringify(keyIds),
                    type: "POST",
				    contentType: "application/json",
                    successCallback: function () {
                        alert('操作成功', function (index) {
                            vm.reload();
                        });
					}
				});
			});
		},
        print: function (event) {
            let keyId = getSelectedRow("#jqGrid");
            if (keyId == null) {
                return;
            }

            var id = jQuery("#jqGrid").jqGrid('getGridParam', 'selrow');
            var ret = jQuery("#jqGrid").jqGrid('getRowData', id);

            var newWeb=window.open('_blank');
            var url = "http://192.168.2.254:8082/glassReport/reportJsp/showReport.jsp?rpx=new_purchase_goods_stock_in.rpx&stockInId="+ret.stockInId;
            newWeb.location=url;

        },
		getInfo: function(keyId){
            Ajax.request({
                url: "../purchasegoodsstockin/info/"+keyId,
                async: true,
                successCallback: function (r) {
                    if (r.code == 0)
                    {
                        vm.purchaseGoodsStockIn = r.purchaseGoodsStockIn;
                        vm.showList = false;
                        vm.title = "修改";
                    }else{
                        //返回主界面
                    }
                }
            });
		},
		reload: function (event) {
			vm.showList = true;
            let page = $("#jqGrid").jqGrid('getGridParam', 'page');
			$("#jqGrid").jqGrid('setGridParam', {
                postData: {'name': vm.q.name},
                page: page
            }).trigger("reloadGrid");
            vm.handleReset('formValidate');
		},
        reloadSearch: function() {
            vm.q = {
                name: ''
            }
            vm.reload();
        },
        handleSubmit: function (name) {
            handleSubmitValidate(this, name, function () {
                vm.saveOrUpdate()
            });
        },
        handleReset: function (name) {
            handleResetForm(this, name);
        },

        /**
         * 获取产品信息
         */
        getProducts: function () {
            Ajax.request({
                url: "../purchasegoodsmain/queryAll",
                async: true,
                successCallback: function (r) {
                    vm.products = r.list;
                }
            });
        },

        /**
         * 获取仓库信息
         */
        getStocks: function () {
            Ajax.request({
                url: "../stockmain/queryAll",
                async: true,
                successCallback: function (r) {
                    vm.stocks = r.list;
                }
            });
        },


        /**
         * 获取强度信息
         */
        /*getHardness: function () {
            Ajax.request({
                url: "../purchasehardness/queryAll",
                async: true,
                successCallback: function (r) {
                    vm.hardnesses = r.list;
                }
            });
        },*/

        /**
         * 初始化数据
         */
        getInitData: function () {
            Ajax.request({
                url: "../purchasegoodsstockin/getInitData",
                async: true,
                successCallback: function (r) {
                    //console.log(r.id);
                    vm.purchaseGoodsStockIn = {stockInId:r.map.id,stockInDate:new Date(),list:[]};
                }
            });
        },
        
        addRow:function () {
            /*let cloneRow = $("#hiddenRow").children("Row").first().clone(true);
            //let cloneRow = $("#hiddenRow").html();
            $("#rowline").append(cloneRow);
            //vm.showList = false;
            console.log($("#rowline").html());*/
            let nowId = this.nextId++;
            this.purchaseGoodsStockIn.list.push({
                itemNumber:'',
                id:nowId,
                productId:'',
                stockQuantity:0,
                itemMemo:'',
                batchNumber:''
            })
        },

        click1:function (obj) {
            if(selectIndex !== -1)
                return;
            //var _that  = obj;
            selectIndex = layer.open({
                type: 2,
                title: '选择物品',
                content: 'purchasegoodsmainforselect.html',
                btn: ['保存', '取消'],
                offset: ['100px', '15%'],
                area: ['1050px', '400px'],
                //zIndex: 19950924,
                maxmin: true,
                yes: function(index, layero) {
                    //var form = layer.getChildFrame('form', index);
                    //var param = form.serialize();
                    var iframeWin = window[layero.find('iframe')[0]['name']];
                    var ret = iframeWin.getProductInfo();//子窗口提交表单（只验证表单，返回false）
                    //console.log(ret);
                    if(ret[0]) {
                        obj.productId = ret[0].productId;
                        obj.pProductName = ret[0].productName;
                        obj.pProductMeasurement = ret[0].productMeasurement;
                        obj.pProductDescription = ret[0].productDescription;
                        obj.pProductMemo = ret[0].productMemo;
                        obj.pProductClass = ret[0].productClass;
                        obj.pGuige = ret[0].guige;
                        obj.pDaima = ret[0].daima;

                        layer.close(index);
                    }else{
                        alert('请选择一项');
                    }
                },

                end: function() {
                    selectIndex = -1;
                }
            });
        }
	}
});

/*
// 定义一个名为 button-counter 的新组件
Vue.component('button-counter', {
    data: function () {
        return {
            count: 0
        }
    },
    template: '<button v-on:click="count++">You clicked me {{ count }} times.</button>'
});

// 定义一个名为 row-add 的新组件
Vue.component('row-add', {
    props: ['productId','stockQuantity','itemMemo'],
    template: '<div id="divDetail" style="text-align: center">'
    +' <Row :gutter="16">'
    +' <i-col span="4"><i-input v-bind:value="productId" v-on:input="$emit(\'input\', $event.target.value)"/></i-col>'
    +' <i-col span="2"><i-input v-bind:value="stockQuantity" v-on:input="$emit(\'input\', $event.target.value)"/></i-col>'
    +' <i-col span="8"><i-input v-bind:value="itemMemo" v-on:input="$emit(\'input\', $event.target.value)"/></i-col>'
    +' <i-col span="10"></i-col>'
    +' </Row>'
    +' </div>'
});*/
