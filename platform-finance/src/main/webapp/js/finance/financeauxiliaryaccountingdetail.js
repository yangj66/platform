$(function () {
    $("#jqGrid").Grid({
        url: '../financeauxiliaryaccountingdetail/list',
        colModel: [
			{label: 'id', name: 'id', index: 'id', key: true, hidden: true},
			{label: '编码', name: 'serialNumber', index: 'serial_number', width: 80},
			{label: '名称', name: 'name', index: 'name', width: 80},
            {
                label: '状态', name: 'useFlag', index: 'use_flag', width: 50,
                formatter: function (value) {
                    return transUseable(value);
                }
            },
			{label: '类别', name: 'typeName', index: 'type_id', width: 80}]
    });
});

let vm = new Vue({
	el: '#rrapp',
	data: {
        showList: true,
        title: null,
		financeAuxiliaryAccountingDetail: {},
		ruleValidate: {
            serialNumber: [
                {required: true, message: '编码不能为空', trigger: 'blur'}
            ],
			name: [
				{required: true, message: '名称不能为空', trigger: 'blur'}
			],
            type: [
                {type: 'number',required: true, message: '类别不能为空', trigger: 'change'}
            ]
		},
		q: {
		    name: ''
            ,type:''
		},
        typeList: []//辅助核算类别
	},
    created: function () {
        // `this` 指向 vm 实例
        console.log('a is: ' + this.a);
        this.getTypeList();
    },
	methods: {
		query: function () {
			vm.reload();
		},
		add: function () {
			vm.showList = false;
			vm.title = "新增";
			vm.financeAuxiliaryAccountingDetail = {useFlag:"on"};//默认状态为已启用

            vm.typeList = [];
            vm.getTypeList();
		},
		update: function (event) {
            let id = getSelectedRow("#jqGrid");
			if (id == null) {
				return;
			}
			vm.showList = false;
            vm.title = "修改";

            /*var promise1 = new Promise(function(resolve, reject) {
                vm.getTypeList();
                console.log("1");
                resolve('Success!');
            });
            promise1.then(function(value) {
                console.log(value);
                console.log("2");
                vm.getInfo(id);
            });*/

            vm.getInfo(id);
            vm.typeList = [];
            vm.getTypeList();
		},
		saveOrUpdate: function (event) {
            let url = vm.financeAuxiliaryAccountingDetail.id == null ? "../financeauxiliaryaccountingdetail/save" : "../financeauxiliaryaccountingdetail/update";
            Ajax.request({
			    url: url,
                params: JSON.stringify(vm.financeAuxiliaryAccountingDetail),
                type: "POST",
			    contentType: "application/json",
                successCallback: function (r) {
                    alert('操作成功', function (index) {
                        vm.reload();
                    });
                }
			});
		},
		del: function (event) {
            let ids = getSelectedRows("#jqGrid");
			if (ids == null){
				return;
			}

			confirm('确定要删除选中的记录？', function () {
                Ajax.request({
				    url: "../financeauxiliaryaccountingdetail/delete",
                    params: JSON.stringify(ids),
                    type: "POST",
				    contentType: "application/json",
                    successCallback: function () {
                        alert('操作成功', function (index) {
                            vm.reload();
                        });
					}
				});
			});
		},
		getInfo: function(id){
            // Ajax.request({
            //     url: "../financeauxiliaryaccountingdetail/info/"+id,
            //     async: true,
            //     successCallback: function (r) {
            //         vm.financeAuxiliaryAccountingDetail = r.financeAuxiliaryAccountingDetail;
            //     }
            // });

            $.get("../financeauxiliaryaccountingdetail/info/" + id, function (r) {
                vm.financeAuxiliaryAccountingDetail = r.financeAuxiliaryAccountingDetail;
                 console.log("vm.financeAuxiliaryAccountingDetail.type=====>"+JSON.stringify(vm.financeAuxiliaryAccountingDetail.type));
            });
		},
		reload: function (event) {
			vm.showList = true;
            let page = $("#jqGrid").jqGrid('getGridParam', 'page');
			$("#jqGrid").jqGrid('setGridParam', {
                postData: {'name': vm.q.name,'type':vm.q.type},
                page: page
            }).trigger("reloadGrid");
            vm.handleReset('formValidate');
		},
        reloadSearch: function() {
            vm.q = {
                name: ''
                ,type: ''
            }
            vm.reload();
        },
        handleSubmit: function (name) {
            handleSubmitValidate(this, name, function () {
                vm.saveOrUpdate()
            });
        },
        handleReset: function (name) {
            handleResetForm(this, name);
        },
        /**
         * 获取辅助核算类型
         */
        getTypeList: function () {
            $.get("../financeauxiliaryaccounting/queryAll", function (r) {
                vm.typeList = r.list;
            });
        },

        enUse: function () {
            var id = getSelectedRow("#jqGrid");
            if (id == null) {
                return;
            }
            confirm('确定要启用选中项？', function () {
                Ajax.request({
                    type: "POST",
                    url: "../financeauxiliaryaccountingdetail/enUse",
                    params: JSON.stringify(id),
                    contentType: "application/json",
                    type: 'POST',
                    successCallback: function () {
                        alert('提交成功', function (index) {
                            vm.reload();
                        });
                    }
                });
            });
        },

        unUse: function () {
            var id = getSelectedRow("#jqGrid");
            if (id == null) {
                return;
            }
            confirm('确定要禁用选中项？', function () {
                Ajax.request({
                    type: "POST",
                    url: "../financeauxiliaryaccountingdetail/unUse",
                    params: JSON.stringify(id),
                    contentType: "application/json",
                    type: 'POST',
                    successCallback: function () {
                        alert('提交成功', function (index) {
                            vm.reload();
                        });
                    }
                });
            });
        }
	}
});