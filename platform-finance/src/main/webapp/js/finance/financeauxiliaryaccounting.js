$(function () {
    $("#jqGrid").Grid({
        url: '../financeauxiliaryaccounting/list',
        colModel: [
			{label: 'id', name: 'id', index: 'id', key: true, hidden: true},
			{label: '类型', name: 'type', index: 'type', width: 80}],
        gridComplete: function() {
            var ids = jQuery("#jqGrid").jqGrid('getDataIDs');
            for ( var i = 0; i < ids.length; i++)
            { var cl = ids[i];
                // be = "<input style='height:22px;width:20px;' type='button' value='E' onclick=\"jQuery('#rowed2').editRow('" + cl + "');\" />";
                // se = "<input style='height:22px;width:20px;' type='button' value='S' onclick=\"jQuery('#rowed2').saveRow('" + cl + "');\" />";
                // ce = "<input style='height:22px;width:20px;' type='button' value='C' onclick=\"jQuery('#rowed2').restoreRow('" + cl + "');\" />";

                xx = "<input style='height:22px;width:20px;' type='button' value='E' onclick=\"alert('" + cl + "');\" />";
                jQuery("#jqGrid").jqGrid('setRowData', ids[i], { act : xx });
            }
        }
    });
});

let vm = new Vue({
	el: '#rrapp',
	data: {
        showList: true,
        title: null,
		financeAuxiliaryAccounting: {},
		ruleValidate: {
			type: [
				{required: true, message: '类型不能为空', trigger: 'blur'}
			]
		},
		q: {
		    name: ''
		}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function () {
			vm.showList = false;
			vm.title = "新增";
			vm.financeAuxiliaryAccounting = {};
		},
		update: function (event) {
            let id = getSelectedRow("#jqGrid");
			if (id == null) {
				return;
			}
			vm.showList = false;
            vm.title = "修改";

            vm.getInfo(id)
		},
		saveOrUpdate: function (event) {
            let url = vm.financeAuxiliaryAccounting.id == null ? "../financeauxiliaryaccounting/save" : "../financeauxiliaryaccounting/update";
            Ajax.request({
			    url: url,
                params: JSON.stringify(vm.financeAuxiliaryAccounting),
                type: "POST",
			    contentType: "application/json",
                successCallback: function (r) {
                    alert('操作成功', function (index) {
                        vm.reload();
                    });
                }
			});
		},
		del: function (event) {
            let ids = getSelectedRows("#jqGrid");
			if (ids == null){
				return;
			}

			confirm('确定要删除选中的记录？', function () {
                Ajax.request({
				    url: "../financeauxiliaryaccounting/delete",
                    params: JSON.stringify(ids),
                    type: "POST",
				    contentType: "application/json",
                    successCallback: function () {
                        alert('操作成功', function (index) {
                            vm.reload();
                        });
					}
				});
			});
		},
		getInfo: function(id){
            Ajax.request({
                url: "../financeauxiliaryaccounting/info/"+id,
                async: true,
                successCallback: function (r) {
                    vm.financeAuxiliaryAccounting = r.financeAuxiliaryAccounting;
                }
            });
		},
		reload: function (event) {
			vm.showList = true;
            let page = $("#jqGrid").jqGrid('getGridParam', 'page');
			$("#jqGrid").jqGrid('setGridParam', {
                postData: {'name': vm.q.name},
                page: page
            }).trigger("reloadGrid");
            vm.handleReset('formValidate');
		},
        reloadSearch: function() {
            vm.q = {
                name: ''
            }
            vm.reload();
        },
        handleSubmit: function (name) {
            handleSubmitValidate(this, name, function () {
                vm.saveOrUpdate()
            });
        },
        handleReset: function (name) {
            handleResetForm(this, name);
        }
	}
});