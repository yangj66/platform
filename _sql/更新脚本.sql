/** 2018-06-21 会员优惠券添加状态字段 **/
ALTER TABLE `nideshop_user_coupon` ADD COLUMN `coupon_status` TINYINT (3) UNSIGNED DEFAULT '1' COMMENT '状态 1. 可用 2. 已用 3. 过期'
/** 2019-07-07 给足迹表添加索引字段 **/
ALTER TABLE `nideshop_footprint`
ADD INDEX `index_nideshop_footprint_user_id_goods_id` (`user_id`, `goods_id`) USING BTREE ;


/*2018-07-05 增加财务模块-- 辅助核算表*/
-- ----------------------------
-- Table structure for finance_auxiliary_accounting
-- ----------------------------
DROP TABLE IF EXISTS `finance_auxiliary_accounting`;
CREATE TABLE `finance_auxiliary_accounting` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for finance_auxiliary_accounting_detail
-- ----------------------------
DROP TABLE IF EXISTS `finance_auxiliary_accounting_detail`;
CREATE TABLE `finance_auxiliary_accounting_detail` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `serial_number` varchar(50) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `use_flag` varchar(5) DEFAULT NULL,
  `type` mediumint(8) DEFAULT NULL COMMENT '辅助核算',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;



/*2019-01 进耗存模块*/
/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50716
Source Host           : localhost:3306
Source Database       : platform-shop

Target Server Type    : MYSQL
Target Server Version : 50716
File Encoding         : 65001

Date: 2019-03-05 09:12:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for purchase_goods_main
-- ----------------------------
DROP TABLE IF EXISTS `purchase_goods_main`;
CREATE TABLE `purchase_goods_main` (
  `key_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `product_id` varchar(50) DEFAULT NULL COMMENT '代码',
  `product_measurement` varchar(10) DEFAULT NULL COMMENT '度量单位',
  `product_name` varchar(100) DEFAULT NULL COMMENT '物品名称',
  `product_description` varchar(500) DEFAULT NULL COMMENT '物品详细描述',
  `product_memo` varchar(50) DEFAULT NULL COMMENT '物品备注',
  `product_class` varchar(50) DEFAULT NULL COMMENT '规格',
  `status` varchar(10) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`key_id`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for purchase_goods_stock_in
-- ----------------------------
DROP TABLE IF EXISTS `purchase_goods_stock_in`;
CREATE TABLE `purchase_goods_stock_in` (
  `key_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `stock_in_id` varchar(50) DEFAULT NULL COMMENT '入库编号',
  `input_date` datetime DEFAULT NULL COMMENT '操作时间',
  `stock_in_date` date DEFAULT NULL COMMENT '入库时间',
  `stock_in_responsier` varchar(50) DEFAULT NULL COMMENT '入库人',
  `stock_id` varchar(10) DEFAULT NULL COMMENT '仓库ID',
  `supplier_id` varchar(50) DEFAULT NULL COMMENT '供应商',
  `stock_in_memo` varchar(500) DEFAULT NULL COMMENT '备注',
  `item_number` tinyint(4) unsigned DEFAULT NULL COMMENT '条目号',
  `product_id` varchar(50) DEFAULT NULL COMMENT '代码',
  `stock_quantity` decimal(10,4) NOT NULL DEFAULT '0.0000' COMMENT '库存数量',
  `item_memo` varchar(500) DEFAULT NULL COMMENT '条目备注',
  `in_quantity` decimal(10,4) DEFAULT NULL COMMENT '入库数量',
  `out_quantity` decimal(10,4) DEFAULT NULL COMMENT '出库数量',
  `status` varchar(10) DEFAULT NULL COMMENT '状态',
  `batch_number` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`key_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for purchase_goods_stock_out
-- ----------------------------
DROP TABLE IF EXISTS `purchase_goods_stock_out`;
CREATE TABLE `purchase_goods_stock_out` (
  `key_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `stock_out_id` varchar(50) DEFAULT NULL COMMENT '出库编号',
  `stock_in_key_id` bigint(20) DEFAULT NULL COMMENT '入库keyId',
  `stock_in_id` varchar(50) DEFAULT NULL COMMENT '入库编号',
  `stock_in_item_number` tinyint(4) DEFAULT NULL COMMENT '入库条目号',
  `stock_out_date` date DEFAULT NULL COMMENT '出库日期',
  `input_date` datetime DEFAULT NULL COMMENT '录入时间',
  `stock_out_responsier` varchar(50) DEFAULT NULL COMMENT '出库责任人',
  `stock_id` varchar(50) DEFAULT NULL COMMENT '仓库ID',
  `stock_out_quantity` decimal(10,4) DEFAULT NULL COMMENT '净重',
  `product_id` varchar(50) DEFAULT NULL COMMENT '物品ID',
  `supplier_id` varchar(50) DEFAULT NULL COMMENT '供应商ID',
  `stock_out_memo` varchar(500) DEFAULT NULL COMMENT '出库备注',
  `withdraw_flag` tinyint(2) DEFAULT NULL COMMENT '冲单标志',
  `stock_out_operator_id` varchar(50) DEFAULT NULL COMMENT '出库操作人',
  `gross_Weight` decimal(10,4) DEFAULT NULL COMMENT '毛重',
  `tare_Weight` double(10,4) DEFAULT NULL COMMENT '皮重',
  `net_Weight` double(10,4) DEFAULT NULL COMMENT '方量',
  `hardness` varchar(50) DEFAULT NULL COMMENT '强度',
  `telephone` varchar(50) DEFAULT NULL COMMENT '联系电话',
  `contacter` varchar(50) DEFAULT NULL COMMENT '联系人',
  `car_id` varchar(50) DEFAULT NULL COMMENT '承运',
  `chauffeur` varchar(50) DEFAULT NULL COMMENT '司机',
  `address` varchar(50) DEFAULT NULL COMMENT '送货地址',
  `project_name` varchar(50) DEFAULT NULL COMMENT '项目名称',
  `unit_price` decimal(10,2) DEFAULT NULL COMMENT '单价',
  `price` decimal(10,2) DEFAULT NULL COMMENT '金额',
  PRIMARY KEY (`key_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for purchase_hardness
-- ----------------------------
DROP TABLE IF EXISTS `purchase_hardness`;
CREATE TABLE `purchase_hardness` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `hardness` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for stock_main
-- ----------------------------
DROP TABLE IF EXISTS `stock_main`;
CREATE TABLE `stock_main` (
  `key_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `stock_id` varchar(10) DEFAULT NULL COMMENT '仓库ID',
  `stock_description` varchar(50) DEFAULT NULL COMMENT '仓库名称',
  `status` varchar(10) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`key_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for supplier_main
-- ----------------------------
DROP TABLE IF EXISTS `supplier_main`;
CREATE TABLE `supplier_main` (
  `key_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `supplier_id` varchar(50) DEFAULT NULL COMMENT '代码',
  `supplier_description` varchar(200) DEFAULT NULL COMMENT '供应商名称',
  `address` varchar(500) DEFAULT NULL COMMENT '地址',
  `tel` varchar(50) DEFAULT NULL COMMENT '电话',
  `status` varchar(10) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`key_id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;



-- ----------------------------
-- Table structure for supplier_main
-- ----------------------------
DROP TABLE IF EXISTS `project_main`;
CREATE TABLE `project_main` (
  `key_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(200) DEFAULT NULL COMMENT '项目名称',
  `address` varchar(500) DEFAULT NULL COMMENT '地址',
  `tel` varchar(50) DEFAULT NULL COMMENT '电话',
  `contact` varchar(50) DEFAULT NULL COMMENT '联系人',
  `status` varchar(10) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`key_id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;




DROP TABLE IF EXISTS `car_id`;
CREATE TABLE `car_id` (
  `key_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `car_id` varchar(50) DEFAULT NULL COMMENT '承运',
  PRIMARY KEY (`key_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `chauffeur`;
CREATE TABLE `chauffeur` (
  `key_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chauffeur` varchar(50) DEFAULT NULL COMMENT '司机',
  PRIMARY KEY (`key_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `out_person`;
CREATE TABLE `out_person` (
  `key_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `out_person` varchar(50) DEFAULT NULL COMMENT '发货人',
  PRIMARY KEY (`key_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

